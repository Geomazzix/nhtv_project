#include "PhysicsGroup.h"
#include <algorithm>
#include <utility>

namespace Vesta
{	
	PhysicsGroup::PhysicsGroup(std::string name) : _Name(std::move(name))
	{

	}

	PhysicsGroup::~PhysicsGroup()
	{
		for (Collider2D* collider : _Colliders)
			collider = nullptr;
		_Colliders.clear();
	}

	void PhysicsGroup::AddCollider(Collider2D* collider)
	{
		if(!ContainsCollider(collider)) _Colliders.emplace_back(collider);
	}

	void PhysicsGroup::RemoveCollider(Collider2D* collider)
	{
		const std::vector<Collider2D*>::iterator itr = std::find(_Colliders.begin(), _Colliders.end(), collider);
		if (itr != _Colliders.end()) _Colliders.erase(itr);
	}

	Collider2D* PhysicsGroup::GetCollider(unsigned int& index)
	{
		if (index > _Colliders.size() - 1) return nullptr;
		return _Colliders[index];
	}

	const bool PhysicsGroup::ContainsCollider(Collider2D* collider)
	{
		const std::vector<Collider2D*>::iterator itr = std::find(_Colliders.begin(), _Colliders.end(), collider);
		return itr != _Colliders.end();
	}
}
