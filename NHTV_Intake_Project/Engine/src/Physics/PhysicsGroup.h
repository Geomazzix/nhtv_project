#pragma once
#include <string>
#include <vector>
#include "../Components/Collider2D.h"

namespace Vesta
{
	///<summary>Handles the </summary>
	class PhysicsGroup
	{
		friend class Collider2D;

	private:
		std::string _Name;
		std::vector<Collider2D*> _Colliders;

	public:
		explicit PhysicsGroup(std::string name);
		~PhysicsGroup();

		void AddCollider(Collider2D* collider);
		void RemoveCollider(Collider2D* collider);

		const int ColliderCount() const { return _Colliders.size(); };
		Collider2D* GetCollider(unsigned int& index);
		const bool ContainsCollider(Collider2D* collider);
		const std::string Name() const { return _Name; }
	};
}
