#pragma once
#include <box2D/box2D.h>
#include "Collider2D.h"

namespace Vesta
{	
	///<summary>A collider in the shape of a circle. Purpose serves for only 2D collision.<summary>
	class Circle2DCollider : public Collider2D
	{
	private:
		b2CircleShape _Shape;

	public:
		Circle2DCollider(const b2Vec2& position, const float& radius, b2Body* body, const bool& active);
		~Circle2DCollider() = default;
	};
}

