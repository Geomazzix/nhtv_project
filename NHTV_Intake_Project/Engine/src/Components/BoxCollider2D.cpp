#include "BoxCollider2D.h"

namespace Vesta
{	
	BoxCollider2D::BoxCollider2D(const b2Vec2& position, const b2Vec2& size, b2Body* body, const float& angle, const bool& active) 
		: Collider2D(body, active)
	{
		_Shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(position.x / 2, position.y / 2), angle);
		_FixtureData.shape = &_Shape;

		switch(body->GetType())
		{
		case b2_staticBody:
			_Fixture = _Body->CreateFixture(&_Shape, 0.0f);
			break;
		case b2_kinematicBody: 
			_Fixture = _Body->CreateFixture(&_Shape, 0.0f);
			break;
		case b2_dynamicBody:
			_Fixture = _Body->CreateFixture(&_FixtureData);
			break;
		default: std::printf("The engine does not support this type of body.\n");
		}
	}
}