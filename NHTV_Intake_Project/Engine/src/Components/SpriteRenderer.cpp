#include "SpriteRenderer.h"

namespace Vesta
{
	SpriteRenderer::SpriteRenderer(Sprite* sprite, const std::string& materialName, const bool& active) : Component(active),
		_MirrorSpriteX(false), _MirrorSpriteY(false)
	{
		_Sprite = sprite;
		SetMaterial(materialName.empty() ? "Diffuse" : materialName);
	}

	SpriteRenderer::~SpriteRenderer()
	{
		delete _Sprite;
	}

	void SpriteRenderer::SetMaterial(const std::string& materialName)
	{
		materialShaders.clear();
		materialShaders.emplace_back(GL_VERTEX_SHADER, std::string("shaders/vertexShaders/" + materialName + ".vts"));
		materialShaders.emplace_back(GL_FRAGMENT_SHADER, std::string("shaders/fragmentShaders/" + materialName + ".fgs"));
		_Material = ResourceManager::LoadMaterial(materialName, materialShaders);
	}

	void SpriteRenderer::SetMirroredX(const bool& flag)
	{
		_MirrorSpriteX = flag;
		_Sprite->SetMirrorX(flag);
	}

	void SpriteRenderer::SetMirroredY(const bool& flag)
	{
		_MirrorSpriteY = flag;
		_Sprite->SetMirrorY(flag);
	}

	void SpriteRenderer::Render(const glm::mat4& mvpMatrix)
	{
		_Material.Bind();
		_Material.SetUniformMat4("u_Transform", mvpMatrix);
		_Material.SetUniform1i("u_TextureSlot", 0);
		_Sprite->Draw(mvpMatrix);
		_Material.Unbind();
	}
}