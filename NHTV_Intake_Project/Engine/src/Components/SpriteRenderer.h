#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../Resources/Sprite.h"
#include "../ComponentSystem/Component.h"

namespace Vesta
{
	class SpriteRenderer : public Component
	{
	private:
		Sprite* _Sprite;
		Material _Material;
		bool _MirrorSpriteX, _MirrorSpriteY;
		std::vector<Shader> materialShaders;

	public:
		explicit SpriteRenderer(Sprite* sprite, const std::string& materialName = "Diffuse", const bool& active = true);
		~SpriteRenderer();

		void SetSprite(Sprite* sprite) { _Sprite = sprite; }
		void SetMaterial(const std::string& materialName);

		void SetMirroredX(const bool& flag);
		bool GetMirroredX() const { return _MirrorSpriteX; }

		void SetMirroredY(const bool& flag);
		bool GetMirroredY() const { return _MirrorSpriteY; }

		void Render(const glm::mat4& mvpMatrix) override;
	};
}