#include "Circle2DCollider.h"


namespace Vesta
{	
	Circle2DCollider::Circle2DCollider(const b2Vec2& position, const float& radius, b2Body* body, const bool& active) : Collider2D(body, active)
	{
		_Shape.m_p = position;
		_Shape.m_radius = radius;
		_FixtureData.shape = &_Shape;
		_FixtureData.restitution = 0.0f;

		switch (body->GetType())
		{
		case b2_staticBody:
			_Fixture = _Body->CreateFixture(&_Shape, 0.0f);
			break;
		case b2_kinematicBody:
			_Fixture = _Body->CreateFixture(&_Shape, 0.0f);
			break;
		case b2_dynamicBody:
			_Fixture = _Body->CreateFixture(&_FixtureData);
			break;
		default: std::printf("The engine does not support this type of body.\n");
		}
	}
}
