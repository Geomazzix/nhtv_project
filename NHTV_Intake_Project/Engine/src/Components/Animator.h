#pragma once
#include "../Resources/Animation.h"
#include "../ComponentSystem/Component.h"

namespace Vesta
{	
	///<summary>Can play a certain animation.</summary>
	class Animator : public Component
	{
	private:
		bool _IsPlayingAnimation;
		Animation* _Animation;

	public:
		Animator(Animation* animation, const bool& playOnAwake, const bool& active = true);
		~Animator();

		void SetAnimation(Animation* animation, const bool& play = true, const bool& overideOldIndex = false);
		void PlayAnimation();
		void StopAnimation();

		const bool IsPlayingAnimation() const { return _IsPlayingAnimation; }
		const Animation* GetAnimation() const { return _Animation; }

		void Update(const float& dt) override;
	};
}

