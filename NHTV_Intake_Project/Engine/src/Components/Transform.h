#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../ComponentSystem/Component.h"

namespace Vesta
{
	class Transform : public Component
	{
	private:
		glm::vec3 _Position;
		glm::vec3 _Rotation;
		glm::vec3 _Scale;

	public:
		explicit Transform(const glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), 
			const glm::vec3 rotation = glm::vec3(0.0f, 0.0f, 0.0f), 
			const glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f), 
			const bool& active = true) : Component(active), _Position(position), _Rotation(rotation), _Scale(scale) {}
		~Transform() = default;

		glm::mat4 Data() const;

		glm::vec3& GetPosition();
		glm::vec3& GetRotation();
		glm::vec3& GetScale();

		void SetPosition(const glm::vec3& position);
		void SetRotation(const glm::vec3& rotation);
		void SetScale(const glm::vec3& scale);
	};
}
