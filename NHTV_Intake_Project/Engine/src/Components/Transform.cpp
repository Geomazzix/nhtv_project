#include "Transform.h"

namespace Vesta
{
	glm::mat4 Transform::Data() const
	{
		const glm::mat4 translationMat = glm::translate(glm::mat4(1.0f), _Position);
		const glm::mat4 scaleMat = glm::scale(glm::mat4(1.0f), _Scale);

		const glm::mat4 rotationXMat = glm::rotate(glm::mat4(1.0f), _Rotation.x, glm::vec3(1, 0, 0));
		const glm::mat4 rotationYMat = glm::rotate(glm::mat4(1.0f), _Rotation.y, glm::vec3(0, 1, 0));
		const glm::mat4 rotationZMat = glm::rotate(glm::mat4(1.0f), _Rotation.z, glm::vec3(0, 0, 1));
		const glm::mat4 rotationMat = rotationZMat * rotationYMat * rotationXMat;

		return translationMat * rotationMat * scaleMat;
	}

	glm::vec3& Transform::GetPosition()
	{
		return _Position;
	}

	glm::vec3& Transform::GetRotation()
	{
		return _Rotation;
	}

	glm::vec3& Transform::GetScale()
	{
		return _Scale;
	}

	void Transform::SetPosition(const glm::vec3& position)
	{
		_Position = position;
	}

	void Transform::SetRotation(const glm::vec3& rotation)
	{
		_Rotation = rotation;
	}

	void Transform::SetScale(const glm::vec3& scale)
	{
		_Scale = scale;
	}
}