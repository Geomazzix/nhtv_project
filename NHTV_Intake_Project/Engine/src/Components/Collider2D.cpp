#include "Collider2D.h"
#include "../World.h"

namespace Vesta
{	
	Collider2D::Collider2D(b2Body* body, const bool& active = true) : Component(active), _Fixture(nullptr), _Body(body)
	{
		_FixtureData.density = 1.0f;
		_FixtureData.friction = 0.3f;
	}

	Collider2D::~Collider2D()
	{
		_Fixture = nullptr;
	}
}
