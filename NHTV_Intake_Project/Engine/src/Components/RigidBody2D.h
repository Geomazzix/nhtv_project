#pragma once
#include <Box2D/Dynamics/b2Body.h>
#include <vector>
#include "../ComponentSystem/Component.h"
#include "Collider2D.h"

namespace Vesta
{	
	///<summary>Handles the properties of a body.</summary>
	class RigidBody2D final : public Component
	{
	private:
		b2BodyDef _BodyData;
		b2Body* _Body;

		std::vector<Collider2D*> _Colliders;

	public:
		RigidBody2D(const b2Vec2& position, const b2BodyType& bodyType, const bool& active = true);
		~RigidBody2D();

		b2Body* Body() const { return _Body; }
	};
}

