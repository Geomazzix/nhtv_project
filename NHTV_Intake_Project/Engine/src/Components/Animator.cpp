#include "Animator.h"


namespace Vesta
{	
	Animator::Animator(Animation* animation, const bool& playOnAwake, const bool& active) 
	: Component(active), _IsPlayingAnimation(playOnAwake), _Animation(animation)
	{
		_Animation->PlayAnimation(playOnAwake);
	}

	Animator::~Animator()
	{
		_Animation = nullptr;
	}

	void Animator::SetAnimation(Animation* animation, const bool& play, const bool& overrideOldIndex)
	{
		if (_Animation->GetName() == animation->GetName() && !overrideOldIndex) return;
		_Animation = animation;
		_Animation->PlayAnimation(play);
	}

	void Animator::PlayAnimation()
	{
		_IsPlayingAnimation = true;
		_Animation->PlayAnimation(true);
	}

	void Animator::StopAnimation()
	{
		_IsPlayingAnimation = false;
		_Animation->PlayAnimation(false);
	}

	void Animator::Update(const float& dt)
	{
		Component::Update(dt);
		if (_Animation == nullptr) return;
		_Animation->Update(dt);
	}
}
