#pragma once
#include <Box2D/Box2D.h>
#include "Collider2D.h"

namespace Vesta
{	
	///<summary>A collider in the shape of a box. Purpose serves for only 2D collision.<summary>
	class BoxCollider2D final : public Collider2D
	{
	private:
		b2PolygonShape _Shape;

	public:
		BoxCollider2D(const b2Vec2& position, const b2Vec2& size, b2Body* body, const float& angle = 0.0f, const bool& active = true);
		~BoxCollider2D() = default;
	};
}