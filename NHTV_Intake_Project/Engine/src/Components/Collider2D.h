#pragma once
#include <Box2D/Box2D.h>
#include "../ComponentSystem/Component.h"

namespace Vesta
{	
	class Collider2D : public Component
	{
	protected:
		b2FixtureDef _FixtureData;
		b2Fixture* _Fixture;
		b2Body* _Body;

		Collider2D(b2Body* body, const bool& active);
		virtual ~Collider2D();

	public:
		b2Fixture* Fixture() const { return _Fixture; }
	};
}

