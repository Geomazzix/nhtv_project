#include "RigidBody2D.h"
#include "../World.h"

namespace Vesta
{	
	RigidBody2D::RigidBody2D(const b2Vec2& position, const b2BodyType& bodyType, const bool& active ) : Component(active), _Body(nullptr)
	{
		_BodyData.position.Set(position.x, position.y);
		_BodyData.type = bodyType;
		_Body = World::PWorld()->CreateBody(&_BodyData);
	}

	RigidBody2D::~RigidBody2D()
	{
		World::PWorld()->DestroyBody(_Body);
	}
}
