#pragma once
#include <GL/glew.h>

namespace Vesta
{
	class ErrorHandler
	{
	public:
		ErrorHandler() = default;
		~ErrorHandler() = default;

		static void glfwErrorCallback(int error, const char* description);
		static void GLAPIENTRY OpenGLErrorCallBack(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);
	};
}
