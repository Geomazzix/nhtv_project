#include "ErrorHandler.h"

#if _DEBUG //Only compile windows.h when debugging because the file is rather large.

#include <cstdio>
#include <string>
#include <windows.h>

namespace Vesta
{	
	void ErrorHandler::glfwErrorCallback(int error, const char* description)
	{
		std::printf("Error: %s\n", description);
	}

	void ErrorHandler::OpenGLErrorCallBack(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
	{
		//Returns at the not important notifications. (Some of them cause spam).
		if (source == 33350) return;

		std::string errorLog = "";
		const HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);

		switch (type)
		{
		case GL_DEBUG_TYPE_ERROR:
			std::printf("GL_ERROR");
			break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			std::printf("GL_DEPRECATED_BEHAVIOR");
			break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			std::printf("GL_UNDEFINED_BEHAVIOR");
			break;
		case GL_DEBUG_TYPE_PORTABILITY:
			std::printf("GL_PORTABILITY");
			break;
		case GL_DEBUG_TYPE_PERFORMANCE:
			std::printf("GL_PERFORMANCE");
			break;
		case GL_DEBUG_TYPE_OTHER:
			std::printf("GL_OTHER");
			break;
		default:
			std::printf("GL_TYPE_NOT_FOUND");
			break;
		}
		std::printf("------------------------------------------------------------\n");
		std::printf("Source: %u\n", source);
		std::printf("ID: %d\n", id);
		switch (severity) {
			case GL_DEBUG_SEVERITY_LOW:
				SetConsoleTextAttribute(console, FOREGROUND_GREEN | FOREGROUND_BLUE);
				std::printf("Severity: LOW\n");
				break;
			case GL_DEBUG_SEVERITY_MEDIUM:
				SetConsoleTextAttribute(console, FOREGROUND_RED | FOREGROUND_GREEN);
				std::printf("Severity: MEDIUM\n");
				break;
			case GL_DEBUG_SEVERITY_HIGH:
				SetConsoleTextAttribute(console, FOREGROUND_RED);
				std::printf("Severity: HIGH\n");
				break;
			case GL_DEBUG_SEVERITY_NOTIFICATION: 
				std::printf("Severity: NOTIFICATION\n");
				break;
			default:
				SetConsoleTextAttribute(console, FOREGROUND_RED);
				std::printf("Severity: _NOT_FOUND_ \n");
		}
		std::printf("Log: %s\n", message);
		SetConsoleTextAttribute(console, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
	}
}
#endif