#include "Window.h"
#include <SOIL2/SOIL2.h>

namespace Vesta
{	
	CursorInput* Window::Cursor;
	KeyboardInput* Window::KeyboardInputManager;


	Window::Window(std::string windowTitle, const unsigned int& width, const unsigned int& height, const WindowState& state, const bool& vSync)
		: _Width(width), _Height(height), _WindowState(WindowState::WINDOWED), _VSync(vSync), _VidMode(nullptr), _Monitor(nullptr), 
		_Window(nullptr), _WindowTitle(std::move(windowTitle))
	{
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		glfwWindowHint(GLFW_RESIZABLE, false);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);
#if _DEBUG
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
#endif

		CreateWindow(_WindowTitle, _Width, _Height, _WindowState, vSync);

		KeyboardInputManager = new KeyboardInput(_Window);
		Cursor = new CursorInput(_Window);
	}

	Window::~Window()
	{
		if (_Window != nullptr) glfwDestroyWindow(_Window);

		delete KeyboardInputManager;
		delete Cursor;
	}

	void Window::SetWindowTitle(const std::string& windowTitle)
	{
		_WindowTitle = windowTitle;
		glfwSetWindowTitle(_Window, windowTitle.c_str());
	}

	void Window::SetWindowIcon(const std::string icons[3]) const
	{
		GLFWimage images[3];
		for(unsigned short i = 0; i < 3; ++i) 
			images[i].pixels = SOIL_load_image(icons[i].c_str(), &images[i].width, &images[i].height, nullptr, SOIL_LOAD_RGBA);
		
		glfwSetWindowIcon(_Window, 3, images);
		
		for (auto& image : images) 
			SOIL_free_image_data(image.pixels);
	}

	void Window::SetWindowState(const WindowState& state) const
	{
		if (_WindowState == state) return;		
		
		if(state != INVISIBLE)
		{
			glfwShowWindow(_Window);
			glfwSetWindowMonitor(
				_Window, state == FULLSCREEN ? _Monitor : nullptr, 
				0, 
				0, 
				state == BORDERLESS || state == FULLSCREEN ? _VidMode->width : _Width, 
				state == BORDERLESS || state == FULLSCREEN ? _VidMode->height : _Height, 
				_VidMode->refreshRate);
			return;
		}

		glfwHideWindow(_Window);
	}

	void Window::SetVSync(const bool& vSync)
	{
		glfwDestroyWindow(_Window);
		_VSync = vSync;
		CreateWindow(_WindowTitle, _Width, _Height, _WindowState, vSync);
	}

	float Window::AspectRatio() const
	{
		return static_cast<float>(_VidMode->width / _VidMode->height);
	}

	void Window::Clear() const
	{
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		switch(_WindowState)
		{
		case WINDOWED: case BORDERLESS:
			glViewport(0, 0, _Width, _Height);
			break;
		case FULLSCREEN:
			glViewport(0, 0, _VidMode->width, _VidMode->height);
			break;
		default: ; //Invisible screens don't need viewport updates.
		}
	}

	void Window::SwapBuffers() const
	{
		if(_VSync) glfwSwapBuffers(_Window);
		else glFlush();
	}

	void Window::PollWindowEvents() const
	{
		glfwPollEvents();
	}

	bool Window::ShouldWindowClose() const
	{
		return glfwWindowShouldClose(_Window);
	}

	void Window::OnWindowFocus(GLFWwindow* window, int select)
	{
		if(select)glfwSetWindowUserPointer(window, window);
		else glfwSetWindowUserPointer(window, nullptr);
	}

	void Window::OnWindowDamage(GLFWwindow* window)
	{
		glfwSwapBuffers(window);
	}
	
	void Window::CreateWindow(std::string& windowTitle, const float& width, const float& height, const WindowState& state, const bool& vSync)
	{
		_Monitor = glfwGetPrimaryMonitor();
		_VidMode = glfwGetVideoMode(_Monitor);

		glfwWindowHint(GLFW_DOUBLEBUFFER, vSync);

		switch (state)
		{
		case WINDOWED:
			glfwWindowHint(GLFW_DECORATED, true);
			_Window = glfwCreateWindow(width, height, windowTitle.c_str(), nullptr, nullptr);
			break;
		case BORDERLESS:
			glfwWindowHint(GLFW_RED_BITS, _VidMode->redBits);
			glfwWindowHint(GLFW_GREEN_BITS, _VidMode->greenBits);
			glfwWindowHint(GLFW_BLUE_BITS, _VidMode->blueBits);
			glfwWindowHint(GLFW_REFRESH_RATE, _VidMode->refreshRate);
			glfwWindowHint(GLFW_DECORATED, false);
			_Window = glfwCreateWindow(_VidMode->width, _VidMode->height, windowTitle.c_str(), _Monitor, nullptr);
			break;
		case FULLSCREEN:
			glfwWindowHint(GLFW_DECORATED, false);
			_Window = glfwCreateWindow(width, height, windowTitle.c_str(), glfwGetPrimaryMonitor(), nullptr);
			glfwSetWindowMonitor(_Window, _Monitor, 0, 0, _VidMode->width, _VidMode->height, _VidMode->refreshRate);
			break;
		case INVISIBLE:
			glfwWindowHint(GLFW_VISIBLE, false);
			_Window = glfwCreateWindow(width, height, windowTitle.c_str(), nullptr, nullptr);
			break;
		default:
			std::printf("failed to create the window named: %s, the given state does not exist.", _WindowTitle.c_str());
			return;
		}

		if (_Window == nullptr)
		{
			std::printf("failed to create the window named: %s", _WindowTitle.c_str());
			glfwTerminate();
			return;
		}

		glfwMakeContextCurrent(_Window);
		glfwSetWindowFocusCallback(_Window, OnWindowFocus);
		glfwSetWindowRefreshCallback(_Window, OnWindowDamage);
	}
}
