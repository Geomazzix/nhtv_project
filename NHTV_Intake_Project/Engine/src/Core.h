#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "ErrorHandler.h"
#include "Window.h"
#include "Events/ApplicationEvents.h"
#include <glm/detail/type_mat4x4.hpp>
#include "Camera2D.h"
#include "ResourceManager.h"
#include "SceneManagement/SceneManager.h"

namespace Vesta
{
	class Core
	{
	private:		
		void GameLoop();
		int _FrameCount;
		bool _Running;
		float _Fps, _LastFpsTime;


	protected:
		Camera2D* _Camera;
		Window* _Window;

	protected:
		Core(const std::string& name, const float& windowWidth, const float& windowHeight, const Window::WindowState& state, const b2Vec2& unitScale, const bool& vSync = false);
		virtual ~Core();

		Update_Event _EFixedUpdate;
		Update_Event _EUpdate;
		Render_Event _ERender;

		virtual void Init();
		virtual void OnRender(const glm::mat4& viewMatrix){}
		virtual void OnUpdate(const float& deltaTime){}
		virtual void OnFixedUpdate(const float& deltaTime){}

	public:
		const double& GetFps() const { return _Fps; }
	};
}

