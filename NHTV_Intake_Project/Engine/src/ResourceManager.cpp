#include "ResourceManager.h"

namespace Vesta
{	
	std::unordered_map<std::string, Texture> ResourceManager::_Textures;
	std::unordered_map<std::string, Material> ResourceManager::_Materials;
	std::unordered_map<std::string, Mesh> ResourceManager::_Meshes;
	std::unordered_map<std::string, TileMap> ResourceManager::_TileMaps;

	Material& ResourceManager::LoadMaterial(const std::string& name, const std::vector<Shader>& shaders)
	{
		if (_Materials.find(name) != _Materials.end())
			return _Materials[name];
		_Materials[name] = Material();
		_Materials[name].Generate(shaders);
		return _Materials[name];
	}

	Material& ResourceManager::GetMaterial(const std::string& name)
	{
		return _Materials[name];
	}

	Texture& ResourceManager::LoadTexture(const std::string& name, const std::string& filePath)
	{
		if (_Textures.find(name) != _Textures.end())
			return _Textures[name];
		_Textures[name] = Texture();
		_Textures[name].Generate(filePath);
		return _Textures[name];
	}

	Texture& ResourceManager::GetTexture(const std::string& name)
	{
		return _Textures[name];
	}

	TileMap& ResourceManager::LoadTileMap(const std::string& name, const std::string& filePath, int tileIndex, const glm::vec2& tileMapDimensions)
	{
		if (_TileMaps.find(name) != _TileMaps.end())
			return _TileMaps[name];
		_TileMaps[name] = TileMap();
		_TileMaps[name].Generate(filePath, tileIndex, tileMapDimensions);
		return _TileMaps[name];
	}

	TileMap& ResourceManager::GetTileMap(const std::string& name)
	{
		return _TileMaps[name];
	}

	Mesh& ResourceManager::LoadMesh(const std::string& name, const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices, const GLenum& type)
	{
		if(_Meshes.find(name) != _Meshes.end())
			return _Meshes[name];
		
		_Meshes[name] = Mesh();
		_Meshes[name].Init(vertices, indices, type);
		return _Meshes[name];
	}

	Mesh& ResourceManager::GetMesh(const std::string& name)
	{
		return _Meshes[name];
	}

	void ResourceManager::ClearResources()
	{
		for (auto& tileMap : _TileMaps) tileMap.second.Delete();
		for (auto& mat : _Materials) mat.second.Delete();
		for (auto& texture : _Textures) texture.second.Delete();
		for (auto& mesh : _Meshes) mesh.second.Delete();
		
		_Materials.clear();
		_Textures.clear();
		_Meshes.clear();
		_TileMaps.clear();
	}
}
