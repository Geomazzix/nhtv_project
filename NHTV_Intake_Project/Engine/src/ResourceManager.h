#pragma once
#include <string>
#include <unordered_map>
#include "Resources/Material.h"
#include "Resources/Mesh.h"
#include "Resources/Texture.h"
#include "Resources/Tilemap.h"

namespace Vesta
{
	class ResourceManager
	{
	private:
		static std::unordered_map<std::string, Texture> _Textures;
		static std::unordered_map<std::string, Material> _Materials;
		static std::unordered_map<std::string, Mesh> _Meshes;
		static std::unordered_map<std::string, TileMap> _TileMaps;

	public:
		static Material& LoadMaterial(const std::string& name, const std::vector<Shader>& shaders);
		static Material& GetMaterial(const std::string& name);
		
		static Texture& LoadTexture(const std::string& name, const std::string& filePath);
		static Texture& GetTexture(const std::string& name);

		static Mesh& LoadMesh(const std::string& name, const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices, const GLenum& type);
		static Mesh& GetMesh(const std::string& name);

		static TileMap& LoadTileMap(const std::string& name, const std::string& filePath, int tileIndex, const glm::vec2& tileMapDimensions);
		static TileMap& GetTileMap(const std::string& name);

		static void ClearResources();
	};
}
