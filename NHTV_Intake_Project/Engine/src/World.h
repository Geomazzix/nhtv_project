#pragma once
#include <Box2D/Dynamics/b2World.h>
#include <unordered_map>
#include "Resources/GameObject.h"
#include "Physics/PhysicsGroup.h"

namespace Vesta
{	
	///<summary>The scene hierarchy which contains pointers to all the gameObjects of the currently bound scene.</summary>
	class World final
	{
		friend class SceneManager;
	private:
		static const int VELOCITY_ITERATIONS = 6;
		static const int POSITION_ITERATIONS = 2;
		static b2World* _PWorld;
		static b2Vec2 _PGravity;
		static std::unordered_map<std::string, GameObject*> _GameObjects;
		static std::unordered_map<std::string, PhysicsGroup*> _PhysicGroups;

	public:
		static void Init();
		static void PhysicsUpdate(const float& deltaTime);
		static void Update(const float& deltaTime);
		static void Render(const glm::mat4& viewMatrix);
		static void Clear();

		static void AddGameObject(const std::string& name, GameObject* gm);
		static void RemoveGameObject(const std::string& name);
		static GameObject* GetGameObject(const std::string& name);

		static b2World* PWorld() { return _PWorld; }
		static b2Vec2 PGravity() { return _PGravity; }

		static void AddPhysicGroup(PhysicsGroup* physicGroup);
		static void RemovePhysicGroup(PhysicsGroup* physicGroup);
		static PhysicsGroup* GetPhysicsGroup(const std::string& name);
	};
}
