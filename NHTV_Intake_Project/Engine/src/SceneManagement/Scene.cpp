#include "Scene.h"

namespace Vesta
{
	Scene::Scene() : _Active(false) {}
	Scene::~Scene() { Clear(); }
	
	void Scene::Bind()
	{
		_Active = true;
		for (auto& gameObject : _GameObjects)
			gameObject.second->SetActive(true);
	}

	void Scene::UnBind()
	{
		_Active = false;
		for (auto& gameObject : _GameObjects)
			gameObject.second->SetActive(false);
	}

	void Scene::Clear()
	{
		for(auto& gameObject : _GameObjects)
		{
			gameObject.second->Delete();
			delete gameObject.second;
		}
		_GameObjects.clear();
	}
}
