#include "SceneManager.h"

namespace Vesta
{
	std::unordered_map<std::string, Scene*> SceneManager::_Scenes;
	Scene* SceneManager::_ActiveScene = nullptr;
	World* SceneManager::_World;

	void SceneManager::ClearHierarchy()
	{
		for (auto& object : Vesta::World::_GameObjects)
			_ActiveScene->_GameObjects.emplace(std::make_pair(object.first, object.second));
		Vesta::World::_GameObjects.clear();
	}

	void SceneManager::BindScene(const std::string& name)
	{
		if(_ActiveScene != nullptr) _ActiveScene->UnBind();
		_ActiveScene = _Scenes[name];
		_ActiveScene->Bind();
	}

	void SceneManager::FillHierarchy()
	{
		for (auto& object : _ActiveScene->_GameObjects)
			Vesta::World::AddGameObject(object.first, object.second);
		_ActiveScene->_GameObjects.clear();
	}

	SceneManager::SceneManager()
	{
		_World = new World();
	}

	SceneManager::~SceneManager()
	{
		for(auto& scene : _Scenes)
		{
			scene.second->Clear();
			delete scene.second;
		}
		_Scenes.clear();
		delete _ActiveScene;
		delete _World;
	}

	void SceneManager::LoadScene(const std::string& name)
	{
		//Check if the scene exists, if not create it.
		if (_Scenes.find(name) == _Scenes.end())
			_Scenes.emplace(name, new Scene());

		ClearHierarchy();
		BindScene(name);
		FillHierarchy();
	}
}
