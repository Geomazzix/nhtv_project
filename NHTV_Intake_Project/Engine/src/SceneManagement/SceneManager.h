#pragma once
#include <unordered_map>
#include "Scene.h"
#include "../Resources/GameObject.h"
#include "../World.h"

namespace Vesta
{	
	///<summary>Handles scene rendering and updating. Also handles scene loading.</summary>
	class SceneManager
	{
	private:
		static World* _World;
		static std::unordered_map<std::string, Scene*> _Scenes;
		static Scene* _ActiveScene;

		static void ClearHierarchy();
		static void BindScene(const std::string& name);
		static void FillHierarchy();

	public:
		SceneManager();
		~SceneManager();

		static void LoadScene(const std::string& name);
	};
}
