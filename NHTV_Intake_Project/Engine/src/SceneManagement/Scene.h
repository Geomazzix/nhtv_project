#pragma once
#include "../Resources/GameObject.h"
#include <string>
#include <map>

namespace Vesta
{	
	///<summary>Handles a map of gameObject, can be bound or unbound. Can also be deleted.</summary>
	struct Scene
	{
		friend class SceneManager;

	private:
		std::map<std::string, GameObject*> _GameObjects;
		bool _Active;
		
		void Clear();
		
		void Bind();
		void UnBind();
	
	public:
		Scene();
		~Scene();
	};
}

