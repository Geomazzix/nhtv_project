#include "VertexBufferLayout.h"
#include "Vertex.h"

namespace Vesta
{
	void VertexBufferLayout::AddAttribute(const GLint& amountInAttribute, const GLenum& type, const GLboolean& normalized, const GLuint& offset)
	{
		_Attributes.emplace_back(_Attributes.size(), amountInAttribute, type, normalized);
		_Offset += offset;

		const int lastIndex = _Attributes.size() - 1;
		glVertexAttribPointer(
			_Attributes[lastIndex].GetId(),
			_Attributes[lastIndex].GetSize(),
			_Attributes[lastIndex].GetType(),
			_Attributes[lastIndex].GetNormalized(),
			sizeof(Vertex),
			reinterpret_cast<void*>(_Offset));
	}

	void VertexBufferLayout::Bind() const
	{
		for (auto& attribute : _Attributes)
			glEnableVertexAttribArray(attribute.GetId());
	}

	void VertexBufferLayout::Unbind() const
	{
		for (auto& attribute : _Attributes)
			glDisableVertexAttribArray(attribute.GetId());
	}

	void VertexBufferLayout::Delete()
	{
		_Attributes.clear();
	}
}
