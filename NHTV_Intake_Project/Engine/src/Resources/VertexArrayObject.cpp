#include "VertexArrayObject.h"

namespace Vesta
{	
	VertexArrayObject::VertexArrayObject() : _Id(0) { }

	void VertexArrayObject::Init()
	{
		glGenVertexArrays(1, &_Id);
	}

	void VertexArrayObject::Delete() const
	{
		glDeleteVertexArrays(1, &_Id);
	}

	void VertexArrayObject::Bind() const
	{
		glBindVertexArray(_Id);
	}

	void VertexArrayObject::Unbind() const
	{
		glBindVertexArray(0);
	}
}
