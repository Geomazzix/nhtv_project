#pragma once
#include <GL/glew.h>
#include <vector>
#include "Vertex.h"
#include "VertexArrayObject.h"
#include "VertexBufferObject.h"
#include "ElementBufferObject.h"
#include "VertexBufferLayout.h"

namespace Vesta
{	
	///<summary>Serves as the base class for other mesh classes but can be used to load meshes from files as well. </summary>
	class Mesh
	{
	private:
		VertexArrayObject _VAO;
		VertexBufferObject _VBO;
		ElementBufferObject _EBO;
		VertexBufferLayout _VBL;

	public:
		Mesh() = default;
		~Mesh() = default;

		void Init(std::vector<Vertex> vertices, std::vector<GLuint> indices, const GLenum& type);
		void Delete();

		void SetVertices(std::vector<Vertex> vertices);

		void Draw() const;

		const GLuint& VAO() const { return _VAO.GetId(); }
	};
}
