#pragma once
#include <GL/glew.h>
#include <string>

namespace Vesta
{
	///<summary>Used for selecting the right textureType in a sprite.</summary>
	enum class TextureType
	{
		Texture2D,
		TileMap
	};

	///<summary>Simple texture class which can be derived from for further more complex intentions.<summary>
	class Texture
	{
	public:
		Texture();
		virtual ~Texture() = default;

		virtual void Generate(const std::string& filePath);
		virtual void Delete();

		void Bind(const unsigned int& slot) const;
		void Unbind() const;

		const std::string& FilePath() const { return _FilePath; }
		const int& Width() const { return _TextureWidth; }
		const int& Height() const { return _TextureHeight; }
		const int& Channels() const { return _TextureChannels; }

	protected:
		std::string _FilePath;
		GLuint _TextureId;
		int _TextureWidth, _TextureHeight, _TextureChannels;
	};
}
