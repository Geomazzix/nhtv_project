#include "VertexBufferObject.h"

namespace Vesta
{
	VertexBufferObject::VertexBufferObject() : _Id(0) {}

	void VertexBufferObject::Init(std::vector<Vertex> vertices, const GLenum& type)
	{
		glGenBuffers(1, &_Id);
		glBindBuffer(GL_ARRAY_BUFFER, _Id);
		_Vertices = vertices;
		_Type = type;
		glBufferData(GL_ARRAY_BUFFER, _Vertices.size() * sizeof(Vertex), _Vertices.data(), _Type);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void VertexBufferObject::Delete()
	{
		_Vertices.clear();
		glDeleteBuffers(1, &_Id);
	}

	void VertexBufferObject::SetVertices(std::vector<Vertex> vertices)
	{
		glBindBuffer(GL_ARRAY_BUFFER, _Id);
		_Vertices = vertices;
		glBufferData(GL_ARRAY_BUFFER, _Vertices.size() * sizeof(Vertex), _Vertices.data(), _Type);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void VertexBufferObject::Bind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, _Id);
	}

	void VertexBufferObject::Unbind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}
