#pragma once
#include "Texture.h"
#include <glm/vec2.hpp>
#include <array>

namespace Vesta
{	
	///<summary>Holds certain texture data and can retrieve parts of it by asking for UV coordinates.<summary>
	class TileMap : public Texture
	{
	private:
		glm::vec2 _TileMapDimensions; //The tile dimension is to specify the width and height of the "tile/frame" inside of the tilemap.
		std::array<glm::vec2, 4> _SelectedUvs;

	public:
		TileMap();
		virtual ~TileMap() = default;

		void Generate(const std::string& filePath, int tileIndex, glm::vec2 tileMapDimensions);
		void Delete() override;

		std::array<glm::vec2, 4> GetUvs(int tileIndex);
	};
}
