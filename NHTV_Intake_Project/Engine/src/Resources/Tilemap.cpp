#include "TileMap.h"


namespace Vesta
{	
	TileMap::TileMap() : Texture(), _TileMapDimensions(1, 1), _SelectedUvs()
	{
		_SelectedUvs[0] = glm::vec2(0, 0);
		_SelectedUvs[1] = glm::vec2(0, 1);
		_SelectedUvs[2] = glm::vec2(1, 1);
		_SelectedUvs[3] = glm::vec2(1, 0);
	}

	void TileMap::Generate(const std::string& filePath, int tileIndex, glm::vec2 tileMapDimensions)
	{
		_TileMapDimensions = tileMapDimensions;
		Texture::Generate(filePath);
		GetUvs(tileIndex);
	}

	void TileMap::Delete()
	{
		Texture::Delete();
	}

	std::array<glm::vec2, 4> TileMap::GetUvs(int tileIndex)
	{
		const glm::vec2 tileCoordinates = glm::vec2(static_cast<int>(tileIndex % static_cast<int>(_TileMapDimensions.x)), static_cast<int>(tileIndex / _TileMapDimensions.x));	
		const glm::vec2 cellDimensions = glm::vec2(1 / _TileMapDimensions.x, 1 / _TileMapDimensions.y);
		const glm::vec2 cellOffset = glm::vec2(cellDimensions.x * tileCoordinates.x, cellDimensions.y * tileCoordinates.y);

		_SelectedUvs[0] = glm::vec2(cellOffset.x, cellOffset.y);										//0,0
		_SelectedUvs[1] = glm::vec2(cellOffset.x, cellOffset.y + cellDimensions.y);						//0,1
		_SelectedUvs[2] = glm::vec2(cellOffset.x + cellDimensions.x, cellOffset.y + cellDimensions.y);	//1,1
		_SelectedUvs[3] = glm::vec2(cellOffset.x + cellDimensions.x , cellOffset.y);					//1,0

		return _SelectedUvs;
	}
}
