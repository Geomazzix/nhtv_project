#include "Sprite.h"

namespace Vesta
{	
	Sprite::Sprite(std::string name) : _Name(name), _Width(0), _Height(0), _MirrorX(false), _MirrorY(false) {}

	void Sprite::Generate(std::string name, float width, float height, std::string texturePath, GLenum meshType, VertexColor color)
	{
		CreateMesh(name, width, height, meshType, color);
		CreateTexture(name, texturePath);
	}

	void Sprite::SetMesh(const Mesh& mesh)
	{
		_Mesh = mesh;
	}

	void Sprite::CreateMesh(std::string name, float width, float height, GLenum meshType, VertexColor color)
	{
		_Width = width;
		_Height = height;

		//Marge of the quad-mesh.
		const glm::vec2 position = glm::vec2(-_Width / 2, -_Height / 2);
		const glm::vec2 surface = glm::vec2(_Width, _Height);

		//Create the vertices for the quad.
		_Vertices.clear();
		_Vertices.emplace_back(glm::vec3(position.x, position.y, 0.0f),							color, glm::vec2(0.0f, 0.0f));
		_Vertices.emplace_back(glm::vec3(position.x, position.y + surface.y, 0.0f),				color, glm::vec2(0.0f, 1.0f));
		_Vertices.emplace_back(glm::vec3(position.x + surface.x, position.y + surface.y, 0.0f), color, glm::vec2(1.0f, 1.0f));
		_Vertices.emplace_back(glm::vec3(position.x + surface.x, position.y, 0.0f),				color, glm::vec2(1.0f, 0.0f));

		//Indices for the quad.
		_Indices.emplace_back(0);
		_Indices.emplace_back(1);
		_Indices.emplace_back(2);
		_Indices.emplace_back(2);
		_Indices.emplace_back(3);
		_Indices.emplace_back(0);

		_Mesh = ResourceManager::LoadMesh(_Name, _Vertices, _Indices, meshType);
	}

	void Sprite::SetTexture(const Texture& texture)
	{
		_Texture = texture;
	}

	void Sprite::CreateTexture(const std::string& name, const std::string& texturePath)
	{
		_Texture = ResourceManager::LoadTexture(name, texturePath);
	}

	void Sprite::SetMirrorX(const bool& flag)
	{
		_MirrorX = flag;
	}

	void Sprite::SetMirrorY(const bool& flag)
	{
		_MirrorY = flag;
	}

	void Sprite::Draw(const glm::mat4& mvpMat) 
	{
		//Do this just before the rendering of the texture and mesh because the animator component changes the UV coordinates to the frame in the animation.
		InverseUvs();

		_Texture.Bind(0);
		_Mesh.Draw();
		_Texture.Unbind();
	}

	void Sprite::SetUvs(std::array<glm::vec2, 4> uvs)
	{
		for (unsigned short i = 0; i < _Vertices.size(); ++i)
			_Vertices[i].Uv = uvs[i];
		_Mesh.SetVertices(_Vertices);
	}

	void Sprite::InverseUvs()
	{
		//Create a copy of the vertices of the mesh because the invsersion would end up in an endless loop.
		std::vector<Vertex> newVertices = _Vertices;

		//Swap the x value of the vertices's uv coordinates.
		if(_MirrorX)
		{
			const float x1 = _Vertices[0].Uv.x;
			const float x2 = _Vertices[1].Uv.x;
			newVertices[0].Uv.x = _Vertices[3].Uv.x;
			newVertices[1].Uv.x = _Vertices[2].Uv.x;
			newVertices[2].Uv.x = x2;
			newVertices[3].Uv.x = x1;
		}

		//Swap the y value of the vertices's uv coordinates.
		if(_MirrorY)
		{			
			const float y1 = _Vertices[0].Uv.y;
			const float y2 = _Vertices[2].Uv.y;
			newVertices[0].Uv.y = _Vertices[1].Uv.y;
			newVertices[1].Uv.y = y1;
			newVertices[2].Uv.y = _Vertices[3].Uv.y;
			newVertices[3].Uv.y = y2;
		}

		_Mesh.SetVertices(newVertices);
	}
}
