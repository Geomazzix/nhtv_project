#pragma once
#include <GL/glew.h>

namespace Vesta
{
	///<summary>Holds the data needed to allocate a vertexAttribute to the VAO.</summary>
	struct VertexAttributeLayout final
	{
	private:
		GLuint _Id;
		GLint _Size;
		GLenum _Type;
		GLboolean _Normalized;

	public:
		VertexAttributeLayout(GLuint id, GLint size, GLenum type, GLboolean normalized)
			: _Id(id), _Size(size), _Type(type), _Normalized(normalized) {}
		~VertexAttributeLayout() = default;

		const GLuint GetId() const { return _Id; }
		const GLint GetSize() const { return _Size; }
		const GLenum GetType() const { return _Type; }
		const GLboolean GetNormalized() const { return _Normalized; }
	};
}
