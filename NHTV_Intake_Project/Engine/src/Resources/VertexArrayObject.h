#pragma once
#include <GL/glew.h>

namespace Vesta
{
	///<summary>Holds the layout of the object and can bind the data to it.</summary>
	class VertexArrayObject final
	{
		GLuint _Id;

	public:
		VertexArrayObject();
		~VertexArrayObject() = default;

		void Init();
		void Delete() const;

		void Bind() const;
		void Unbind() const;

		const int GetId() const { return _Id; }
	};
}
