#include "Texture.h"
#include <SOIL2/SOIL2.h>
#include <cstdio>
#include "../ResourceManager.h"

namespace Vesta
{	
	Texture::Texture() : _FilePath(""), _TextureId(0), _TextureWidth(0), _TextureHeight(0), _TextureChannels(0) {}

	void Texture::Generate(const std::string& filePath)
	{
		_FilePath = filePath;
		unsigned char* textureData = SOIL_load_image(_FilePath.c_str(), &_TextureWidth, &_TextureHeight, &_TextureChannels, SOIL_LOAD_RGBA);

		//Allocate the data buffer.
		glGenTextures(1, &_TextureId);
		glBindTexture(GL_TEXTURE_2D, _TextureId);

		//Set the wrapping/filtering options. (tile-able and blurry).
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		if (!textureData)
		{
			textureData = SOIL_load_image("../Engine/textures/no_texture.jpg", &_TextureWidth, &_TextureHeight, &_TextureChannels, SOIL_LOAD_RGBA);
			if (!textureData) std::printf("Failed to load texture data, also failed to load no_texture.jpg");
		}

		//Set the texture data.
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, _TextureWidth, _TextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);
		glGenerateMipmap(GL_TEXTURE_2D);

		//Unbind the texture and free the image data.
		glBindTexture(GL_TEXTURE_2D, 0);
		SOIL_free_image_data(textureData);
	}

	void Texture::Delete()
	{
		glDeleteTextures(1, &_TextureId);
	}

	void Texture::Bind(const unsigned int& slot) const
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glActiveTexture(GL_TEXTURE0 + slot);
		glBindTexture(GL_TEXTURE_2D, _TextureId);
	}

	void Texture::Unbind() const
	{
		glBindTexture(GL_TEXTURE_2D, 0);
		glDisable(GL_BLEND);
	}
}
