#include "Animation.h"


namespace Vesta
{	
	Animation::Animation(std::string name) : _Name(name), _Sprite(nullptr), _TileMap(nullptr), _AnimationSpeed(1), _SpriteCount(0), _SpriteIndex(0), 
	_SpriteStartIndex(0), _IsPlaying(false), _Loop(false), _FrameIntervalTimer(0) {}

	void Animation::Init(Sprite* sprite, TileMap* tileMap, unsigned int spriteCount, unsigned int startFrameIndex)
	{
		_Sprite = sprite;
		_TileMap = tileMap;
		_SpriteCount = spriteCount;
		_Sprite->SetUvs(_TileMap->GetUvs(startFrameIndex));
		_SpriteStartIndex = startFrameIndex;
		_SpriteIndex = startFrameIndex;
	}

	Animation::~Animation()
	{
		_TileMap = nullptr;
	}

	void Animation::SetAnimationSpeed(const float& speed)
	{
		if(speed == 0 && _IsPlaying)
		{
			_IsPlaying = false;
			return;
		}

		_AnimationSpeed = speed;
		_IsPlaying = true;
	}

	void Animation::Update(const float& deltaTime)
	{
		if (!_IsPlaying) return;
		_FrameIntervalTimer -= deltaTime;
		if (_FrameIntervalTimer > 0) return;

		_FrameIntervalTimer = FRAME_INTERVAL_TIMER / _AnimationSpeed;
		_SpriteIndex = _SpriteIndex >= _SpriteCount + _SpriteStartIndex - 1
						? _Loop ? _SpriteStartIndex : _SpriteCount + _SpriteStartIndex - 1
						: _SpriteIndex + 1;
		_Sprite->SetUvs(_TileMap->GetUvs(_SpriteIndex));
	}

	void Animation::PlayAnimation(const bool& flag)
	{
		_IsPlaying = flag;
		_SpriteIndex = _SpriteStartIndex;
	}

	void Animation::SetLoop(const bool& flag)
	{
		_Loop = flag;
	}
}
