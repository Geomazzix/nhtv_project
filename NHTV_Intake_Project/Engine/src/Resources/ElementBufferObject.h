#pragma once
#include <GL/glew.h>
#include <vector>

namespace Vesta
{	
	class ElementBufferObject final
	{
	private:
		GLuint _Id;
		GLenum _Type;
		std::vector<GLuint> _Indices;

	public:
		ElementBufferObject();
		~ElementBufferObject() = default;

		void Init(std::vector<GLuint> indices, const GLenum& type);
		void Delete();

		void Bind() const;
		void Unbind() const;

		const int& GetIndicesCount() const { return _Indices.size(); }
		const GLuint& GetId() const { return _Id; }
		const GLenum& GetType() const { return _Type; }
	};
}