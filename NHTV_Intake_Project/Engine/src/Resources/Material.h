#pragma once
#include "GL/glew.h"
#include <glm/mat4x4.hpp>
#include <vector>
#include <string>
#include <unordered_map>
#include "../IOManager.h"

namespace Vesta
{	
	///<summary> Keeps track of the data used to instantiate the material, contains file paths and shader types </summary>
	struct Shader
	{
	private:
		GLenum _ShaderType;
		std::string _ShaderPath;

	public:
		Shader(const GLenum type, const std::string& path)
		{
			_ShaderType = type;
			_ShaderPath = path;
		}

		std::string GetShaderPath() const
		{
			return _ShaderPath;
		}

		GLenum GetShaderType() const
		{
			return _ShaderType;
		}
	};

	///<summary>Keeps track of a shader program which has multiple shaders attached to it.</summary>
	class Material
	{
	protected:
		enum CompiledObjectType
		{
			shader,
			program
		};

		std::vector<Shader> _ShaderData;
		GLuint _ProgramId;

		GLint GetUniformLocation(const std::string& uniformName);
		std::unordered_map<std::string, GLint> _UniformLocations;

		GLuint CreateShader(const Shader& shader) const;
		bool CheckCompileErrors(GLuint id, CompiledObjectType type) const;
		
	public:
		Material();
		~Material() = default;

		void Generate(std::vector<Shader> shaderData);
		void Delete();

		void Bind() const;
		void Unbind() const;

		void SetUniform1f(const std::string& name, const float value);
		void SetUniform1i(const std::string& name, const int value);
		void SetUniformMat4(const std::string& name, const glm::mat4& mat);
	};
}
