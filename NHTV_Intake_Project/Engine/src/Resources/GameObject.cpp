#include "GameObject.h"
#include "../World.h"

namespace Vesta
{
	GameObject::GameObject(const std::string& name, const bool& active) : Entity(name, active)
	{
		_Transform = &AddComponent<Transform>(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), true);
		World::AddGameObject(name, this);
	}

	void GameObject::OnEnable()
	{
		Entity::OnEnable();
		World::AddGameObject(_Name, this);
	}

	void GameObject::OnDisable()
	{
		Entity::OnDisable();
		World::RemoveGameObject(_Name);
	}

	void GameObject::Render(const glm::mat4& viewMatrix)
	{
		Entity::Render(viewMatrix * _Transform->Data());
	}

	void GameObject::Update(const float& deltaTime)
	{
		Entity::Update(deltaTime);
	}

	void GameObject::Delete()
	{
		Entity::Delete();
		_Transform = nullptr;
		delete _Transform;
	}
}
