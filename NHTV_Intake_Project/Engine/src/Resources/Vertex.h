#pragma once
#include <GL/glew.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace Vesta
{
	///<summary>Holds the color data of a vertex</summary>
	struct VertexColor final
	{
		GLubyte R, G, B, A;

		VertexColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a)
			: R(0), G(0), B(0), A(1)
		{
			R = r;
			G = g;
			B = b;
			A = a;
		}
	};

	///<summary>Represents a single vertex of a mesh.</summary>
	struct Vertex final
	{
		glm::vec3 Position;
		VertexColor Color;
		glm::vec2 Uv;

		Vertex(glm::vec3 position, VertexColor color, glm::vec2 uv)
			: Position(0, 0, 0), Color(0, 0, 0, 1), Uv(0, 0)
		{
			Position = position;
			Color = color;
			Uv = uv;
		}
	};
}
