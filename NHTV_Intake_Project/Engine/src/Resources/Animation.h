#pragma once
#include "Tilemap.h"
#include "Sprite.h"
#include <string>

namespace Vesta
{	
	///<summary>An animation is a tilemap that changes the UV coordinates with a certain interval.</summary>
	class Animation
	{
	private:
		std::string _Name;
		Sprite* _Sprite;
		TileMap* _TileMap;
		float  _AnimationSpeed;
		unsigned int _SpriteCount, _SpriteIndex, _SpriteStartIndex;
		bool _IsPlaying, _Loop;
		float _FrameIntervalTimer;
		const float FRAME_INTERVAL_TIMER = 1.0f; //The default sprite update time is 1 sprite/sec

	public:
		Animation(std::string name);
		~Animation();

		void Init(Sprite* sprite, TileMap* tileMap, unsigned int spriteCount, unsigned int startFrameIndex = 1);
		void SetAnimationSpeed(const float& speed);
		void PlayAnimation(const bool& flag);
		void SetLoop(const bool& flag);

		void Update(const float& deltaTime);

		const std::string GetName() const { return _Name; }
		const float GetAnimationSpeed() const { return _AnimationSpeed; }
		const bool IsPlaying() const { return _IsPlaying; }
		const int GetSpriteCount() const { return _SpriteCount; }
	};
}
