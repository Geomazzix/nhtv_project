#pragma once
#include "Vertex.h"
#include <GL/glew.h>
#include <vector>

namespace Vesta
{
	class VertexBufferObject final
	{
	private:
		GLuint _Id;
		GLenum _Type;
		std::vector<Vertex> _Vertices;

	public:
		VertexBufferObject();
		~VertexBufferObject() = default;

		void Init(std::vector<Vertex> vertices, const GLenum& type);
		void Delete();

		void SetVertices(std::vector<Vertex> vertices);

		void Bind() const;
		void Unbind() const;

		const GLuint& GetId() const { return _Id; }
		const GLenum& GetType() const { return _Type; }
	};
}
