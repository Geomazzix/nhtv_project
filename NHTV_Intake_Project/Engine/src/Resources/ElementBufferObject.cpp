#include "ElementBufferObject.h"

namespace Vesta
{	
	ElementBufferObject::ElementBufferObject() : _Id(0), _Type(GL_STATIC_DRAW) {}

	void ElementBufferObject::Init(std::vector<GLuint> indices, const GLenum& type)
	{

		glGenBuffers(1, &_Id);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _Id);
		_Indices = indices;
		_Type = type;
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * _Indices.size(), _Indices.data(), _Type);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	void ElementBufferObject::Delete()
	{
		_Indices.clear();
		glDeleteBuffers(1, &_Id);
	}

	void ElementBufferObject::Bind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _Id);
	}

	void ElementBufferObject::Unbind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
}