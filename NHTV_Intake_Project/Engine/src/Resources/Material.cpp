#include "Material.h"
#include <utility>
#include "../ResourceManager.h"

namespace Vesta
{	
	Material::Material() : _ProgramId(0) {};

	void Material::Generate(std::vector<Shader> shaderData)
	{
		_ShaderData = shaderData;

		_ProgramId = glCreateProgram();
		if (_ProgramId == 0)
		{
			std::printf("Failed to create material program.");
			return;
		}

		//Create a buffer to store the shaderIds.
		const unsigned int shaderCount = _ShaderData.size();
		std::vector<GLuint> shaderIds;
		shaderIds.reserve(shaderCount);

		//Create, compile and attach the shaders to the shader program.
		for (unsigned int i = 0; i < shaderCount; i++)
		{
			GLuint shaderId = CreateShader(_ShaderData[i]);
			if (shaderId == 0) continue;
			
			glCompileShader(shaderId);
			if (!CheckCompileErrors(shaderId, CompiledObjectType::shader))
			{
				for (int j = i - 1; j >= 0; j--)
					glDeleteShader(shaderIds[j]);
				shaderIds.clear();
				return;
			}

			glAttachShader(_ProgramId, shaderId);
			shaderIds.push_back(shaderId);
		}

		//If shader creation went successful link the program.
		glLinkProgram(_ProgramId);
		if (!CheckCompileErrors(_ProgramId, CompiledObjectType::program))
			glDeleteProgram(_ProgramId);

		//After linking make sure to delete all the shaders.
		for (unsigned int& shaderId : shaderIds)
		{
			glDetachShader(_ProgramId, shaderId);
			glDeleteShader(shaderId);
		}
		shaderIds.clear();
	}

	void Material::Delete()
	{
		if (_ProgramId != 0) glDeleteProgram(_ProgramId);
		_ShaderData.clear();
	}

	GLuint Material::CreateShader(const Shader& shader) const
	{
		std::string shaderPath = shader.GetShaderPath();
		const GLuint shaderId = glCreateShader(shader.GetShaderType());
		if (shaderId == 0)
		{
			std::printf("Shader creation of %s failed!", shaderPath.c_str());
			return 0;
		}

		std::string source = IOManager::LoadShaderSourceFromFile(shaderPath.c_str());
		const char* shaderSource = source.c_str();
		glShaderSource(shaderId, 1, &shaderSource, nullptr);
		
		return shaderId;
	}

	bool Material::CheckCompileErrors(GLuint id, CompiledObjectType type) const
	{
		GLchar infoLog[1024];
		GLint success = 0;

		switch(type)
		{
		case shader:
			glGetShaderiv(id, GL_COMPILE_STATUS, &success);
			if (success) return true;
			glGetShaderInfoLog(id, 1024, nullptr, infoLog);
			std::printf("%s", infoLog);
			break;
		case program:
			glGetProgramiv(id, GL_LINK_STATUS, &success);
			if (success) return true;
			glGetProgramInfoLog(id, 1024, nullptr, infoLog);
			std::printf("%s", infoLog);
			break;
		default:
			assert("That is not a valid type to check for.");
			break;
		}
		return false;
	}


	void Material::Bind() const
	{
		glUseProgram(_ProgramId);
	}

	void Material::Unbind() const
	{
		glUseProgram(0);
	}


	GLint Material::GetUniformLocation(const std::string& uniformName)
	{
		if (_UniformLocations.find(uniformName) != _UniformLocations.end())
			return _UniformLocations[uniformName];

		GLint uniformLocation = glGetUniformLocation(_ProgramId, uniformName.c_str());
		if (uniformLocation == -1) std::printf("Could not find a uniform named: %s\n", uniformName.c_str());

		_UniformLocations.insert(std::make_pair(uniformName, uniformLocation));
		return uniformLocation;
	}

	void Material::SetUniform1f(const std::string& name, const float value)
	{
		glUniform1f(GetUniformLocation(name), value);
	}

	void Material::SetUniform1i(const std::string& name, const int value)
	{
		glUniform1i(GetUniformLocation(name), value);
	}

	void Material::SetUniformMat4(const std::string& name, const glm::mat4& mat)
	{
		glUniformMatrix4fv(GetUniformLocation(name), 1, GL_FALSE, &mat[0][0]);
	}
}
