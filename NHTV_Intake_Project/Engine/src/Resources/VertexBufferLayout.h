#pragma once
#include "VertexAttributeLayout.h"
#include <deque>
#include "Vertex.h"

namespace Vesta
{
	///<summary>Keeps track of the data inside of the VAO.</summary>
	class VertexBufferLayout
	{
	private:
		std::deque<VertexAttributeLayout> _Attributes;
		int _Offset;

	public:
		VertexBufferLayout() : _Offset(0){}
		~VertexBufferLayout() = default;

		void AddAttribute(const GLint& amountInAttribute, const GLenum& type, const GLboolean& normalized, const GLuint& offset);

		void Bind() const;
		void Unbind() const;

		void Delete();

		const int GetAttributeCount() const { return _Attributes.size(); }
	};
}
