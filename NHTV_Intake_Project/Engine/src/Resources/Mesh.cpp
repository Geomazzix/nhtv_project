#include <GL/glew.h>
#include "Mesh.h"

namespace Vesta
{
	void Mesh::Init(std::vector<Vertex> vertices, std::vector<GLuint> indices, const GLenum& type)
	{
		_VAO.Init();
		_VAO.Bind();

		_VBO.Init(vertices, type);
		_VBO.Bind();

		_EBO.Init(indices, type);
		_EBO.Bind();

		_VBL.AddAttribute(3, GL_FLOAT,			GL_FALSE,	0);
		_VBL.AddAttribute(4, GL_UNSIGNED_BYTE,	GL_TRUE,	sizeof(glm::vec3));
		_VBL.AddAttribute(2, GL_FLOAT,			GL_FALSE,	sizeof(VertexColor));
		_VBL.Bind();

		_VAO.Unbind();
	}

	void Mesh::Delete()
	{
		_VAO.Unbind();
		_VBO.Delete();
		_EBO.Delete();
		_VBL.Delete();
		_VAO.Delete();
	}

	void Mesh::SetVertices(std::vector<Vertex> vertices)
	{
		_VAO.Bind();
		_VBO.SetVertices(vertices);
		_VAO.Unbind();
	}

	void Mesh::Draw() const
	{
		_VAO.Bind();
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
		_VAO.Unbind();
	}
}
