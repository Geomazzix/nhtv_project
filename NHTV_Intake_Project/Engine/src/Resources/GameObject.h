#pragma once
#include <string>
#include <glm/glm.hpp>
#include <unordered_map>
#include "../Components/Transform.h"
#include "../Components/SpriteRenderer.h"
#include "../ComponentSystem/Entity.h"

namespace Vesta
{
	///<summary>Serves as an object which can be created by the user, which will be automatically added to the Scene hierarchy.</summary>
	class GameObject : public Entity
	{
	protected:
		Transform* _Transform;
		void OnEnable() override;
		void OnDisable() override;

	public:
		GameObject(const std::string& name, const bool& active = true);
		~GameObject() = default;

		void Render(const glm::mat4& viewMatrix) override;
		void Update(const float& deltaTime) override;
		void Delete() override;
	};
}
