#pragma once
#include "../ResourceManager.h"
#include "Texture.h"
#include "Mesh.h"
#include <array>

namespace Vesta
{	
	///<summary>A sprite is a 2D drawable presentation of a texture, drawn on a quad. It can be drawn using the sprite renderer component.
	///The size is defined by the texture width and height.</summary>
	class Sprite
	{

	private:
		std::string _Name;
		Texture _Texture;
		Mesh _Mesh;

		int _Width, _Height;
		std::vector<Vertex> _Vertices;
		std::vector<GLuint> _Indices;
		bool _MirrorX, _MirrorY;

		void InverseUvs();

	public:
		explicit Sprite(std::string name);
		~Sprite() = default;

		void Generate(std::string name, float width, float height, std::string texturePath, GLenum meshType = GL_STATIC_DRAW, VertexColor color = VertexColor(255, 255, 255, 255));
		void SetMesh(const Mesh& mesh);
		void CreateMesh(std::string name, float width, float height, GLenum meshType = GL_STATIC_DRAW, VertexColor color = VertexColor(255, 255, 255, 255));
		void SetTexture(const Texture& texture);
		void CreateTexture(const std::string& name, const std::string& texturePath);

		std::string FilePath() const { return _Texture.FilePath(); }
		int Width() const { return _Width; }
		int Height() const { return _Height; }

		void SetMirrorX(const bool& flag);
		void SetMirrorY(const bool& flag);

		void Draw(const glm::mat4& mvpMat);
		void SetUvs(std::array<glm::vec2, 4> uvs);
};
}

