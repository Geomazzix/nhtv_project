#include "Core.h"

namespace Vesta
{
	Core::Core(const std::string& name, const float& windowWidth, const float& windowHeight, const Window::WindowState& state, const b2Vec2& unitScale, const bool& vSync) 
	: _FrameCount(0), _Running(true), _Fps(0), _LastFpsTime(0), _Window(nullptr)
	{
		//Initialize GLFW and if running the debug build also the error handler.
#if _DEBUG
		glfwSetErrorCallback(Vesta::ErrorHandler::glfwErrorCallback);
#endif
		if (glfwInit() == GLFW_FALSE) return;

		//Log the current GLFW library version.
		int major, minor, revision;
		glfwGetVersion(&major, &minor, &revision);
		std::printf("Running against GLFW %i.%i.%i\n", major, minor, revision);

		//Create the application window.
		_Window = new Window(name, windowWidth, windowHeight, state, vSync);

		//Initialize GLEW and log it's version.
		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK) return;
		fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

		//When running the debug build enable GLEW error callback.
#if _DEBUG
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(ErrorHandler::OpenGLErrorCallBack, nullptr);
		GLuint unusedIds = 0;
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, &unusedIds, true);
#endif

		//Instantiate a camera, because without a camera no rendering. (The user can make more camera's if he wants but there will always be one active).
		World::Init();
		_Camera = new Vesta::Camera2D(glm::vec3(0.0f, 0.0f, 1.0f), windowWidth, windowHeight, unitScale.x, unitScale.y);
		_Camera->Resize(windowWidth, windowHeight);
	}

	Core::~Core()
	{
		ResourceManager::ClearResources();
		World::Clear();
		delete _Camera;
		delete _Window;
		glfwTerminate();
	}

	void Core::Init()
	{
		GameLoop();
	}

	void Core::GameLoop()
	{
		const double physicsTick = 1.0f / _Window->GetRefreshRate();
		double currentTime = glfwGetTime();
		double physicsTickTimer = 0.0f;

		while (_Running)
		{
			//Keep track of the window state.
			_Running = !_Window->ShouldWindowClose();
			if (!_Running) continue;

			//FrameTime calculation.
			const double startFrameTime = glfwGetTime();
			double deltaTime = startFrameTime - currentTime;
			if (deltaTime > 0.2f) deltaTime = 0.2f; //Clamp the physics to make sure they don't fall behind.
			currentTime = startFrameTime;

			//Keep track of the FPS.
			++_FrameCount;
			if (startFrameTime - _LastFpsTime >= 1.0f)
			{
				_Fps = _FrameCount;
				_FrameCount = 0;
				_LastFpsTime = currentTime;
			}

			//Update.
			World::Update(deltaTime);
			_EUpdate.Invoke(deltaTime);
			
			//Physics.
			physicsTickTimer += deltaTime;
			while(physicsTickTimer >= physicsTick)
			{
				World::PhysicsUpdate(physicsTick);
				_EFixedUpdate.Invoke(physicsTick);
				physicsTickTimer -= physicsTick;
			}

			//Render
			_Window->Clear();
			World::Render(_Camera->ViewMatrix());
			_ERender.Invoke(_Camera->ViewMatrix());
			_Window->SwapBuffers();
			_Window->PollWindowEvents();
		}
	}
}
