#pragma once
#include <string>

namespace Vesta
{	
	class IOManager
	{
	public:
		IOManager() = default;
		~IOManager() = default;

		static std::string LoadShaderSourceFromFile(const char* filePath);
		//std::string LoadTextureSourceFromFile(const char* filePath);
	};
}
