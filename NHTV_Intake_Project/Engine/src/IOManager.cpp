#include "IOManager.h"
#include <fstream>
#include <sstream>

namespace Vesta
{	
	std::string IOManager::LoadShaderSourceFromFile(const char* filePath)
	{
		std::string shaderSource = "";
		try
		{
			std::ifstream shaderSourceReader(filePath);
			if (!shaderSourceReader.is_open()) return "";

			std::stringstream shaderStream;
			shaderStream << shaderSourceReader.rdbuf();
			shaderSourceReader.close();
		
			shaderSource = shaderStream.str();
		}
		catch(std::exception& exc)
		{
			std::printf("Failed to read shader at %s\n EXCEPTION: %s", filePath, exc.what());
		}
		return shaderSource;
	}
}
