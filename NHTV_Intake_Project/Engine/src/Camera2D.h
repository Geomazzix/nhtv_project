#pragma once
#include <glm/gtc/type_ptr.hpp>

namespace Vesta
{
	class Camera2D
	{
	private:
		glm::vec3 _Position, _Up, _Forward;
		glm::mat4 ProjectionMatrix() const { return glm::ortho(-_Width / 2, _Width / 2, -_Height / 2, _Height / 2, _ZNear, _ZFar); }
		float _ZNear, _ZFar, _Width, _Height, _InternWidth, _InternHeight;

	public:
		Camera2D(const glm::vec3 position, const float width, const float height, const float internWidth, const float internHeight) 
		: _Position(glm::vec3(position)), _Up(0.0f, 1.0f, 0.0f), _Forward(0.0, 0.0f, -1.0f), _ZNear(0.01f), _ZFar(10.0f), 
		_Width(width), _Height(height), _InternWidth(internWidth), _InternHeight(internHeight) {}
		~Camera2D() = default;

		glm::mat4 ViewMatrix() const { return ProjectionMatrix() * glm::lookAt(_Position, _Position + _Forward, _Up); }

		float GetZNear() const { return _ZNear; };
		void SetZNear(const float zNear) { _ZNear = zNear; };
		
		float GetZFar() const { return _ZFar; };
		void SetZFar(const float zFar) { _ZFar = zFar; };

		glm::vec2 GetPosition() const { return glm::vec2(_Position.x, _Position.y); };
		void SetPosition(const glm::vec2 position) { _Position = glm::vec3(position.x, position.y, _Position.z); };

		float& Width() { return _Width; }
		float& Height() { return _Height; }
		void Resize(int width, int height)
		{
			_Height = _InternWidth / width * height;
			_Width =  _InternHeight / height * width;
		}
	};
}