#pragma once
#include "VestaEvent.h"
#include <functional>

namespace Vesta
{	
	///<summary>Base class for input events.</summary>
	class AMazz_InputEvent
	{
	protected:
		AMazz_InputEvent() = default;

	public:
		virtual ~AMazz_InputEvent() = default;
		virtual void Invoke() = 0;
	};

	#pragma region Keyboard events 

	///<summary>Base class for keyboard input events.</summary>
	class KeyboardKey_Event : public AMazz_InputEvent
	{
	protected:
		explicit KeyboardKey_Event(const int& glfwKeyCode) : AMazz_InputEvent(), _KeyCode(glfwKeyCode) {}
		int _KeyCode;
	
	public:
		using keyboardKeyDelegate = std::function<void()>;
		VestaEvent<keyboardKeyDelegate>* keyboardEvent = new VestaEvent<keyboardKeyDelegate>();
		virtual ~KeyboardKey_Event() { delete keyboardEvent; }
	
		int Keycode() const { return _KeyCode; }
	};

	///<summary>Keyboard event for when a key is being pressed.</summary>
	class KeyboardKey_Pressed_Event : public KeyboardKey_Event
	{
	public:
		explicit KeyboardKey_Pressed_Event(const int& glfwKeyCode) : KeyboardKey_Event(glfwKeyCode) {}
		virtual ~KeyboardKey_Pressed_Event() = default;

		void Invoke() override
		{
			for (const auto& subscriber : keyboardEvent->Subscribers())
				subscriber();
		}
	};

	///<summary>Keyboard event for when a key is being held down.</summary>
	class KeyboardKey_HeldDown_Event : public KeyboardKey_Event
	{
	public:
		explicit KeyboardKey_HeldDown_Event(const int& glfwKeyCode) : KeyboardKey_Event(glfwKeyCode) {}
		virtual ~KeyboardKey_HeldDown_Event() = default;

		void Invoke() override
		{
			for (const auto& subscriber : keyboardEvent->Subscribers())
				subscriber();
		}
	};

	///<summary>Keyboard event for when a key is being released.</summary>
	class KeyboardKey_Released_Event : public KeyboardKey_Event
	{
	public:
		explicit KeyboardKey_Released_Event(const int& glfwKeyCode) : KeyboardKey_Event(glfwKeyCode) {}
		virtual ~KeyboardKey_Released_Event() = default;

		void Invoke() override
		{
			for (const auto& subscriber : keyboardEvent->Subscribers())
				subscriber();
		}
	};

	#pragma endregion
}