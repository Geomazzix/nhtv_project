#pragma once
#include <vector>
#include <unordered_map>

namespace Vesta
{
	template<typename T>
	class VestaEvent
	{
	private:
		std::unordered_map <std::string, T> _Subscribers;

	public:
		VestaEvent() = default;
		~VestaEvent() { _Subscribers.clear(); }

		void Subscribe(const std::string& methodName, T method)
		{
			if (_Subscribers.find(methodName) == _Subscribers.end())
				_Subscribers[methodName] = method;
		}
		void UnSubscribe(const std::string& methodName)
		{
			if (_Subscribers.find(methodName) != _Subscribers.end())
				_Subscribers.erase(methodName);
		}

		std::vector<T> Subscribers()
		{
			std::vector<T> subscribers;
			subscribers.reserve(_Subscribers.size());
			for (auto subscriber : _Subscribers)
				subscribers.push_back(subscriber.second);
			return subscribers;
		}
	};
}