#pragma once
#include "VestaEvent.h"
#include <functional>
#include <glm/detail/type_mat4x4.hpp>

namespace Vesta
{
	///<summary>Update event that gets called whenever the application needs to be updated.</summary>
	class Update_Event
	{
	private:
		using UpdateFn = std::function<void(const float& deltaTime)>;

	public:
		Update_Event() = default;
		~Update_Event() = default;

		VestaEvent<UpdateFn>* Event = new VestaEvent<UpdateFn>();

		void Invoke(const float& deltaTime) const
		{
			for (auto& subscriber : Event->Subscribers())
				subscriber(deltaTime);
		};
	};

	///<summary>Render event that gets called whenever the application needs to be rendered.</summary>
	class Render_Event
	{
	private:
		using RenderFn = std::function<void(const glm::mat4& viewMatrix)>;

	public:
		Render_Event() = default;
		~Render_Event() = default;

		VestaEvent<RenderFn>* Event = new VestaEvent<RenderFn>();

		void Invoke(const glm::mat4& viewMatrix) const
		{
			for (auto& subscriber : Event->Subscribers())
				subscriber(viewMatrix);
		};
	};
}