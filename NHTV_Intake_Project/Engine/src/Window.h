#pragma once
#include <GLFW/glfw3.h>
#include <string>
#include "Input/KeyboardInput.h"
#include "Input/CursorInput.h"

namespace Vesta
{
	///<summary>Holds all the data required to render a window using GLFW.</summary>
	class Window
	{
		friend class CursorInput;
		friend class KeyboardInput;

	public:
		enum WindowState
		{
			WINDOWED = 0,
			BORDERLESS,
			FULLSCREEN,
			INVISIBLE
		};

	private:
		int _Width, _Height;
		WindowState _WindowState;
		bool _VSync;
		
		const GLFWvidmode* _VidMode;
		GLFWmonitor* _Monitor;
		GLFWwindow* _Window;
		std::string _WindowTitle;

		void CreateWindow(std::string& windowTitle, const float& width, const float& height, const WindowState& state, const bool& vSync);

	public:
		Window(std::string windowTitle, const unsigned int& width, const unsigned int& height, const WindowState& state, const bool& vSync = true);
		~Window();

		const std::string& GetWindowTitle() const { return _WindowTitle; }
		void SetWindowTitle(const std::string& windowTitle);

		void SetWindowIcon(const std::string icons[3]) const;

		const WindowState& GetWindowState() const { return _WindowState; }
		void SetWindowState(const WindowState& state) const;

		const int& GetRefreshRate() const { return _VidMode->refreshRate; }

		void SetVSync(const bool& vSync);

		float AspectRatio() const;
		void Clear() const;
		void SwapBuffers() const;
		void PollWindowEvents() const;
		bool ShouldWindowClose() const;

		static void OnWindowFocus(GLFWwindow* window, int select);
		static void OnWindowDamage(GLFWwindow* window);

		static CursorInput* Cursor;
		static KeyboardInput* KeyboardInputManager;
	};
}
