#include "World.h"
#include "Components/RigidBody2D.h"
#include <Box2D/Common/b2Settings.h>
#include "Components/BoxCollider2D.h"

namespace Vesta
{	
	b2World* World::_PWorld;
	b2Vec2 World::_PGravity;
	std::unordered_map<std::string, GameObject*> World::_GameObjects;
	std::unordered_map<std::string, PhysicsGroup*> World::_PhysicGroups;

	void World::Init()
	{
		_PGravity = b2Vec2(0.0f, -9.8f);
		_PWorld = new b2World(_PGravity);
	}

	void World::PhysicsUpdate(const float& deltaTime)
	{
		_PWorld->Step(deltaTime, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
		
		Transform* transform = nullptr;
		RigidBody2D* rigidBody = nullptr;

		for (auto& gm : _GameObjects)
		{
			transform = &gm.second->GetComponent<Transform>();
			rigidBody = &gm.second->GetComponent<RigidBody2D>();

			if (transform == nullptr || rigidBody == nullptr) continue;

			transform->GetPosition().x = rigidBody->Body()->GetPosition().x;
			transform->GetPosition().y = rigidBody->Body()->GetPosition().y;
			transform->GetRotation().z = rigidBody->Body()->GetAngle();

			transform = nullptr;
		}

		transform = nullptr;
		rigidBody = nullptr;
	}

	void World::Update(const float& deltaTime)
	{
		for (auto& gameObject : _GameObjects)
		{
			if (!gameObject.second->isActive()) continue;
			gameObject.second->Update(deltaTime);
		}
	}

	void World::Render(const glm::mat4& viewMatrix)
	{
		for (auto& gameObject : _GameObjects)
		{
			if (!gameObject.second->isActive()) continue;
			gameObject.second->Render(viewMatrix);
		}
	}

	void World::Clear()
	{
		for(auto& gameObject : _GameObjects)
		{
			gameObject.second->Delete();
			delete gameObject.second;
		}
		_GameObjects.clear();

		for(auto& physicsGroup : _PhysicGroups)
			delete physicsGroup.second;
		_PhysicGroups.clear();
	}

	void World::AddGameObject(const std::string& name, GameObject* gm)
	{
		if (_GameObjects.find(name) == _GameObjects.end())
			_GameObjects.emplace(name, gm);
	}

	void World::RemoveGameObject(const std::string& name)
	{
		if (_GameObjects.find(name) != _GameObjects.end())
			_GameObjects.erase(name);
	}

	GameObject* World::GetGameObject(const std::string& name)
	{
		return _GameObjects.find(name) != _GameObjects.end() ? _GameObjects[name] : nullptr;
	}

	void World::AddPhysicGroup(PhysicsGroup* physicGroup)
	{
		if (_PhysicGroups.find(physicGroup->Name()) == _PhysicGroups.end())
			_PhysicGroups.emplace(physicGroup->Name(), physicGroup);
	}

	void World::RemovePhysicGroup(PhysicsGroup* physicGroup)
	{
		if (_PhysicGroups.find(physicGroup->Name()) != _PhysicGroups.end())
			_PhysicGroups.erase(physicGroup->Name());
	}

	PhysicsGroup* World::GetPhysicsGroup(const std::string& name)
	{
		return _PhysicGroups.find(name) != _PhysicGroups.end() ? _PhysicGroups[name] : nullptr;
	}
}
