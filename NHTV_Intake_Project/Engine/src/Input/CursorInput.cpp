#include "CursorInput.h"


namespace Vesta
{	
	bool CursorInput::_WithinApplicationWindow;
	double CursorInput::_PosX, CursorInput::_PosY, CursorInput::_ScrollXOffset, CursorInput::_ScrollYOffset;
	std::map<int, int> CursorInput::_MouseButtons = {
		{ GLFW_MOUSE_BUTTON_LEFT,	0 },
		{ GLFW_MOUSE_BUTTON_RIGHT,	0 },
		{ GLFW_MOUSE_BUTTON_MIDDLE, 0 },
		{ GLFW_MOUSE_BUTTON_4, 0 },
		{ GLFW_MOUSE_BUTTON_5, 0 },
		{ GLFW_MOUSE_BUTTON_6, 0 },
		{ GLFW_MOUSE_BUTTON_7, 0 },
		{ GLFW_MOUSE_BUTTON_8, 0 },
	};


	CursorInput::CursorInput(GLFWwindow* window, const std::string& image) : _Window(nullptr), _Cursor(nullptr)
	{
		_Window = window;

		if(image.empty()) _Cursor = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
		else SetCursorImage(image);

		//Set mouse input modes.
		glfwSetInputMode(_Window, GLFW_STICKY_MOUSE_BUTTONS, 1);
		glfwSetInputMode(_Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		glfwSetCursor(_Window, _Cursor);

		//Set mouse callbacks.
		glfwSetCursorEnterCallback(_Window, MouseHover_Callback);
		glfwSetCursorPosCallback(_Window, MousePosition_Callback);
		glfwSetMouseButtonCallback(_Window, MouseButton_Callback);
		glfwSetScrollCallback(_Window, MouseScroll_Callback);
	}

	CursorInput::~CursorInput()
	{
		glfwDestroyCursor(_Cursor);
	}


	void CursorInput::SetCursorMode(int mode) const
	{
		switch(mode)
		{
		case GLFW_CURSOR_DISABLED:
			glfwSetInputMode(_Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			break;
		case GLFW_CURSOR_HIDDEN:
			glfwSetInputMode(_Window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
			break;
		case GLFW_CURSOR_NORMAL:
			glfwSetInputMode(_Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			break;
		default: std::printf("The engine does not support this mode.");
		}
	}

	void CursorInput::SetCursorImage(const std::string& image)
	{
		GLFWimage mouseImage;
		mouseImage.pixels = SOIL_load_image(image.c_str(), &mouseImage.width, &mouseImage.height, nullptr, SOIL_LOAD_RGBA);
		_Cursor = glfwCreateCursor(&mouseImage, 0, 0);
		if (_Cursor == nullptr)
		{
			std::printf("Failed to create cursor!");
			return;
		}
		SOIL_free_image_data(mouseImage.pixels);
	}

	void CursorInput::SetCursorImage(const CursorType& type) const
	{
		switch(type)
		{
		case CursorType::ARROW: 			
			glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
			break;
		case CursorType::CROSSHAIR: 			
			glfwCreateStandardCursor(GLFW_CROSSHAIR_CURSOR);
			break;
		case CursorType::IBEAM: 			
			glfwCreateStandardCursor(GLFW_IBEAM_CURSOR);
			break;
		case CursorType::HAND: 			
			glfwCreateStandardCursor(GLFW_HAND_CURSOR);
			break;
		case CursorType::H_RESIZE: 			
			glfwCreateStandardCursor(GLFW_HRESIZE_CURSOR);
			break;
		case CursorType::V_RESIZE: 			
			glfwCreateStandardCursor(GLFW_VRESIZE_CURSOR);
			break;
		}
	}


	void CursorInput::MouseHover_Callback(GLFWwindow* window, int event)
	{
		_WithinApplicationWindow = event;
	}

	void CursorInput::MousePosition_Callback(GLFWwindow* window, double x, double y)
	{
		_PosX = x;
		_PosY = y;
	}

	void CursorInput::MouseButton_Callback(GLFWwindow* window, int keyCode, int action, int mods)
	{
		_MouseButtons[keyCode] = action;
	}

	void CursorInput::MouseScroll_Callback(GLFWwindow* window, double offsetX, double offsetY)
	{
		_ScrollXOffset = offsetX;
		_ScrollYOffset = offsetY;
	}
}
