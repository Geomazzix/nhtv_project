#pragma once
#include "../Window.h"
#include <GLFW/glfw3.h>
#include <string>
#include <unordered_map>
#include "../Events/InputEvents.h"

#define INPUT_METHOD(method) [&]() {##method();}

namespace Vesta
{
	///<summary>One of the keys on the user's keyboard, which contains 3 events for it's state</summary>
	class Keyboard_Key
	{
		int _State;
		bool _IsPressed;
		KeyboardKey_Pressed_Event _EOnPressed;
		KeyboardKey_HeldDown_Event _EWhilePressed;
		KeyboardKey_Released_Event _EOnRelease;

	public:
		explicit Keyboard_Key(const int& keycode) : _State(0), _EOnPressed(keycode), _EWhilePressed(keycode), _EOnRelease(keycode) {}
		~Keyboard_Key() = default;

		int GetState() const { return _State; }
		bool IsPressed() const { return _IsPressed; }

		//Subscribe methods.
		void SubscribeToOnPressed(const std::string& methodName, const KeyboardKey_Event::keyboardKeyDelegate& method) const { _EOnPressed.keyboardEvent->Subscribe(methodName, method); }
		void SubscribeToWhilePressed(const std::string& methodName, const KeyboardKey_Event::keyboardKeyDelegate& method) const { _EWhilePressed.keyboardEvent->Subscribe(methodName, method); }
		void SubscribeToOnRelease(const std::string& methodName, const KeyboardKey_Event::keyboardKeyDelegate& method) const { _EOnRelease.keyboardEvent->Subscribe(methodName, method); }

		//Unsubcribe methods.
		void UnsubscribeToOnPressed(const std::string& methodName) const { _EOnPressed.keyboardEvent->UnSubscribe(methodName); }
		void UnsubscribeToWhilePressed(const std::string& methodName) const { _EWhilePressed.keyboardEvent->UnSubscribe(methodName); }
		void UnsubscribeToOnRelease(const std::string& methodName) const { _EOnRelease.keyboardEvent->UnSubscribe(methodName); }

		//Invoke methods.
		void InvokeOnPressed()
		{
			_IsPressed = true;
			_State = GLFW_PRESS;
			_EOnPressed.Invoke();
		}
		void InvokePressed()
		{
			_IsPressed = true;
			_State = GLFW_REPEAT;
			_EWhilePressed.Invoke();
		}
		void InvokeOnRelease()
		{
			_IsPressed = false;
			_State = GLFW_RELEASE;
			_EOnRelease.Invoke();
		}
	};


	///<summary>Handles all keyboard input, including text typing.</summary>
	class KeyboardInput
	{
	private:
		GLFWwindow* _Window;
		static std::unordered_map<int, Keyboard_Key*> _KeyboardEvents;
		static void KeyboardKey_Callback(GLFWwindow* window, int keyCode, int scanCode, int action, int mods);

	public:
		explicit KeyboardInput(GLFWwindow* window);
		~KeyboardInput();

		std::string GetKeyName(const int& keyCode) const { return glfwGetKeyName(keyCode, 0); }

		const Keyboard_Key& GetkeyEvent(const int& keyCode) const { return *_KeyboardEvents[keyCode]; }
	};
}
