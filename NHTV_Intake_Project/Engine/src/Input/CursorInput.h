#pragma once
#include "../Window.h"
#include <GLFW/glfw3.h>
#include <string>
#include <SOIL2/SOIL2.h>
#include <map>

namespace Vesta
{

	///<summary>The supported build-in mouse icons of windows</summary>
	enum class CursorType
	{
		ARROW = 0,
		CROSSHAIR,
		IBEAM,
		HAND,
		H_RESIZE,
		V_RESIZE,
	};


	///<summary>Holds all the data of the mouse, should be at all time accessible. Should be updated.</summary>
	class CursorInput
	{
	private:
		GLFWwindow* _Window;
		GLFWcursor* _Cursor;

		static bool _WithinApplicationWindow;
		static double _PosX, _PosY, _ScrollXOffset, _ScrollYOffset;

		static const int MAX_SUPPORTED_MOUSE_BUTTON_COUNT = 9;
		static std::map<int, int> _MouseButtons; //<keycode, buttonState>

		static void MouseHover_Callback(GLFWwindow* window, int event);
		static void MousePosition_Callback(GLFWwindow* window, double x, double y);
		static void MouseButton_Callback(GLFWwindow* window, int keyCode, int action, int mods);
		static void MouseScroll_Callback(GLFWwindow* window, double offsetX, double offsetY);

	public:
		explicit CursorInput(GLFWwindow* window, const std::string& image = "");
		~CursorInput();

		int GetCursorMode() const { return glfwGetInputMode(_Window, GLFW_CURSOR); }
		void SetCursorMode(int mode) const;

		double GetPosX() const { return _PosX; }
		double GetPosY() const { return _PosY; }
		double GetScrollOffsetX() const { return _ScrollXOffset; }
		double GetScrollOffsetY() const { return _ScrollYOffset; }
		bool HoverOverApplicationWindow() const { return _WithinApplicationWindow; }
		
		bool GetMouseButtonState(int buttonId) const { return _MouseButtons[buttonId]; }
		bool GetLeftMouseButtonState() const { return _MouseButtons[GLFW_MOUSE_BUTTON_LEFT]; }
		bool GetRightMouseButtonState() const { return _MouseButtons[GLFW_MOUSE_BUTTON_RIGHT]; }
		bool GetMiddleMouseButtonState() const { return _MouseButtons[GLFW_MOUSE_BUTTON_MIDDLE]; }

		void SetCursorImage(const std::string& image);
		void SetCursorImage(const CursorType& type) const;
	};
}
