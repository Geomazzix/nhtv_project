#include "KeyboardInput.h"

namespace Vesta
{	
	std::unordered_map<int, Keyboard_Key*> KeyboardInput::_KeyboardEvents = {
		{ GLFW_KEY_UNKNOWN, new Keyboard_Key(GLFW_KEY_UNKNOWN) },

		//Sentence devider keys.
		{ GLFW_KEY_SPACE, new Keyboard_Key(GLFW_KEY_SPACE) },
		{ GLFW_KEY_APOSTROPHE, new Keyboard_Key(GLFW_KEY_APOSTROPHE) },
		{ GLFW_KEY_COMMA, new Keyboard_Key(GLFW_KEY_COMMA) },
		{ GLFW_KEY_MINUS, new Keyboard_Key(GLFW_KEY_MINUS) },
		{ GLFW_KEY_PERIOD, new Keyboard_Key(GLFW_KEY_PERIOD) },
		{ GLFW_KEY_SLASH, new Keyboard_Key(GLFW_KEY_SLASH) },

		//Upper number-row keys.
		{ GLFW_KEY_0, new Keyboard_Key(GLFW_KEY_0) },
		{ GLFW_KEY_1, new Keyboard_Key(GLFW_KEY_1) },
		{ GLFW_KEY_2, new Keyboard_Key(GLFW_KEY_2) },
		{ GLFW_KEY_3, new Keyboard_Key(GLFW_KEY_3) },
		{ GLFW_KEY_4, new Keyboard_Key(GLFW_KEY_4) },
		{ GLFW_KEY_5, new Keyboard_Key(GLFW_KEY_5) },
		{ GLFW_KEY_6, new Keyboard_Key(GLFW_KEY_6) },
		{ GLFW_KEY_7, new Keyboard_Key(GLFW_KEY_7) },
		{ GLFW_KEY_8, new Keyboard_Key(GLFW_KEY_8) },
		{ GLFW_KEY_9, new Keyboard_Key(GLFW_KEY_9) },
		{ GLFW_KEY_SEMICOLON, new Keyboard_Key(GLFW_KEY_SEMICOLON) },
		{ GLFW_KEY_EQUAL, new Keyboard_Key(GLFW_KEY_EQUAL) },

		//Alphabet keys.
		{ GLFW_KEY_A, new Keyboard_Key(GLFW_KEY_A) },
		{ GLFW_KEY_B, new Keyboard_Key(GLFW_KEY_B) },
		{ GLFW_KEY_C, new Keyboard_Key(GLFW_KEY_C) },
		{ GLFW_KEY_D, new Keyboard_Key(GLFW_KEY_D) },
		{ GLFW_KEY_E, new Keyboard_Key(GLFW_KEY_E) },
		{ GLFW_KEY_F, new Keyboard_Key(GLFW_KEY_F) },
		{ GLFW_KEY_G, new Keyboard_Key(GLFW_KEY_G) },
		{ GLFW_KEY_H, new Keyboard_Key(GLFW_KEY_H) },
		{ GLFW_KEY_I, new Keyboard_Key(GLFW_KEY_I) },
		{ GLFW_KEY_J, new Keyboard_Key(GLFW_KEY_J) },
		{ GLFW_KEY_K, new Keyboard_Key(GLFW_KEY_K) },
		{ GLFW_KEY_L, new Keyboard_Key(GLFW_KEY_L) },
		{ GLFW_KEY_M, new Keyboard_Key(GLFW_KEY_M) },
		{ GLFW_KEY_N, new Keyboard_Key(GLFW_KEY_N) },
		{ GLFW_KEY_O, new Keyboard_Key(GLFW_KEY_O) },
		{ GLFW_KEY_P, new Keyboard_Key(GLFW_KEY_P) },
		{ GLFW_KEY_Q, new Keyboard_Key(GLFW_KEY_Q) },
		{ GLFW_KEY_R, new Keyboard_Key(GLFW_KEY_R) },
		{ GLFW_KEY_S, new Keyboard_Key(GLFW_KEY_S) },
		{ GLFW_KEY_T, new Keyboard_Key(GLFW_KEY_T) },
		{ GLFW_KEY_U, new Keyboard_Key(GLFW_KEY_U) },
		{ GLFW_KEY_V, new Keyboard_Key(GLFW_KEY_V) },
		{ GLFW_KEY_W, new Keyboard_Key(GLFW_KEY_W) },
		{ GLFW_KEY_X, new Keyboard_Key(GLFW_KEY_X) },
		{ GLFW_KEY_Y, new Keyboard_Key(GLFW_KEY_Y) },
		{ GLFW_KEY_Z, new Keyboard_Key(GLFW_KEY_Z) },

		//Printable misc keys.
		{ GLFW_KEY_LEFT_BRACKET, new Keyboard_Key(GLFW_KEY_LEFT_BRACKET) },
		{ GLFW_KEY_BACKSLASH, new Keyboard_Key(GLFW_KEY_BACKSLASH) },
		{ GLFW_KEY_RIGHT_BRACKET, new Keyboard_Key(GLFW_KEY_RIGHT_BRACKET) },
		{ GLFW_KEY_GRAVE_ACCENT, new Keyboard_Key(GLFW_KEY_GRAVE_ACCENT) },
		{ GLFW_KEY_WORLD_1, new Keyboard_Key(GLFW_KEY_WORLD_1) },
		{ GLFW_KEY_WORLD_2, new Keyboard_Key(GLFW_KEY_WORLD_2) },


		//Functional keys.
		{ GLFW_KEY_ESCAPE, new Keyboard_Key(GLFW_KEY_ESCAPE) },
		{ GLFW_KEY_ENTER, new Keyboard_Key(GLFW_KEY_ENTER) },
		{ GLFW_KEY_TAB, new Keyboard_Key(GLFW_KEY_TAB) },
		{ GLFW_KEY_BACKSPACE, new Keyboard_Key(GLFW_KEY_BACKSPACE) },
		{ GLFW_KEY_INSERT, new Keyboard_Key(GLFW_KEY_INSERT) },
		{ GLFW_KEY_DELETE, new Keyboard_Key(GLFW_KEY_DELETE) },
		{ GLFW_KEY_RIGHT, new Keyboard_Key(GLFW_KEY_RIGHT) },
		{ GLFW_KEY_LEFT, new Keyboard_Key(GLFW_KEY_LEFT) },
		{ GLFW_KEY_DOWN, new Keyboard_Key(GLFW_KEY_DOWN) },
		{ GLFW_KEY_UP, new Keyboard_Key(GLFW_KEY_UP) },
		{ GLFW_KEY_PAGE_UP, new Keyboard_Key(GLFW_KEY_PAGE_UP) },
		{ GLFW_KEY_PAGE_DOWN, new Keyboard_Key(GLFW_KEY_PAGE_DOWN) },
		{ GLFW_KEY_HOME, new Keyboard_Key(GLFW_KEY_HOME) },
		{ GLFW_KEY_END, new Keyboard_Key(GLFW_KEY_END) },
		{ GLFW_KEY_CAPS_LOCK, new Keyboard_Key(GLFW_KEY_CAPS_LOCK) },
		{ GLFW_KEY_SCROLL_LOCK, new Keyboard_Key(GLFW_KEY_SCROLL_LOCK) },
		{ GLFW_KEY_NUM_LOCK, new Keyboard_Key(GLFW_KEY_NUM_LOCK) },
		{ GLFW_KEY_PRINT_SCREEN, new Keyboard_Key(GLFW_KEY_PRINT_SCREEN) },
		{ GLFW_KEY_PAUSE, new Keyboard_Key(GLFW_KEY_PAUSE) },

		//F-- Keys.
		{ GLFW_KEY_F1, new Keyboard_Key(GLFW_KEY_F1) },
		{ GLFW_KEY_F2, new Keyboard_Key(GLFW_KEY_F2) },
		{ GLFW_KEY_F3, new Keyboard_Key(GLFW_KEY_F3) },
		{ GLFW_KEY_F4, new Keyboard_Key(GLFW_KEY_F4) },
		{ GLFW_KEY_F5, new Keyboard_Key(GLFW_KEY_F5) },
		{ GLFW_KEY_F6, new Keyboard_Key(GLFW_KEY_F6) },
		{ GLFW_KEY_F7, new Keyboard_Key(GLFW_KEY_F7) },
		{ GLFW_KEY_F8, new Keyboard_Key(GLFW_KEY_F8) },
		{ GLFW_KEY_F9, new Keyboard_Key(GLFW_KEY_F9) },
		{ GLFW_KEY_F10, new Keyboard_Key(GLFW_KEY_F10) },
		{ GLFW_KEY_F11, new Keyboard_Key(GLFW_KEY_F11) },
		{ GLFW_KEY_F11, new Keyboard_Key(GLFW_KEY_F11) },
		{ GLFW_KEY_F11, new Keyboard_Key(GLFW_KEY_F11) },
		{ GLFW_KEY_F12, new Keyboard_Key(GLFW_KEY_F12) },
		{ GLFW_KEY_F13, new Keyboard_Key(GLFW_KEY_F13) },
		{ GLFW_KEY_F14, new Keyboard_Key(GLFW_KEY_F14) },
		{ GLFW_KEY_F15, new Keyboard_Key(GLFW_KEY_F15) },
		{ GLFW_KEY_F16, new Keyboard_Key(GLFW_KEY_F16) },
		{ GLFW_KEY_F17, new Keyboard_Key(GLFW_KEY_F17) },
		{ GLFW_KEY_F18, new Keyboard_Key(GLFW_KEY_F18) },
		{ GLFW_KEY_F19, new Keyboard_Key(GLFW_KEY_F19) },
		{ GLFW_KEY_F20, new Keyboard_Key(GLFW_KEY_F20) },
		{ GLFW_KEY_F21, new Keyboard_Key(GLFW_KEY_F21) },
		{ GLFW_KEY_F22, new Keyboard_Key(GLFW_KEY_F22) },
		{ GLFW_KEY_F23, new Keyboard_Key(GLFW_KEY_F23) },
		{ GLFW_KEY_F24, new Keyboard_Key(GLFW_KEY_F24) },
		{ GLFW_KEY_F25, new Keyboard_Key(GLFW_KEY_F25) },

		//Keypad keys.
		{ GLFW_KEY_KP_0, new Keyboard_Key(GLFW_KEY_KP_0) },
		{ GLFW_KEY_KP_1, new Keyboard_Key(GLFW_KEY_KP_1) },
		{ GLFW_KEY_KP_2, new Keyboard_Key(GLFW_KEY_KP_2) },
		{ GLFW_KEY_KP_3, new Keyboard_Key(GLFW_KEY_KP_3) },
		{ GLFW_KEY_KP_4, new Keyboard_Key(GLFW_KEY_KP_4) },
		{ GLFW_KEY_KP_5, new Keyboard_Key(GLFW_KEY_KP_5) },
		{ GLFW_KEY_KP_6, new Keyboard_Key(GLFW_KEY_KP_6) },
		{ GLFW_KEY_KP_7, new Keyboard_Key(GLFW_KEY_KP_7) },
		{ GLFW_KEY_KP_8, new Keyboard_Key(GLFW_KEY_KP_8) },
		{ GLFW_KEY_KP_9, new Keyboard_Key(GLFW_KEY_KP_9) },

		//Functional misc keys.
		{ GLFW_KEY_KP_DECIMAL, new Keyboard_Key(GLFW_KEY_KP_DECIMAL) },
		{ GLFW_KEY_KP_DIVIDE, new Keyboard_Key(GLFW_KEY_KP_DIVIDE) },
		{ GLFW_KEY_KP_MULTIPLY, new Keyboard_Key(GLFW_KEY_KP_MULTIPLY) },
		{ GLFW_KEY_KP_SUBTRACT, new Keyboard_Key(GLFW_KEY_KP_SUBTRACT) },
		{ GLFW_KEY_KP_ADD, new Keyboard_Key(GLFW_KEY_KP_ADD) },
		{ GLFW_KEY_KP_ENTER, new Keyboard_Key(GLFW_KEY_KP_ENTER) },
		{ GLFW_KEY_KP_EQUAL, new Keyboard_Key(GLFW_KEY_KP_EQUAL) },

		//Modifier keys
		{ GLFW_KEY_LEFT_SHIFT, new Keyboard_Key(GLFW_KEY_LEFT_SHIFT) },
		{ GLFW_KEY_LEFT_CONTROL, new Keyboard_Key(GLFW_KEY_LEFT_CONTROL) },
		{ GLFW_KEY_LEFT_ALT, new Keyboard_Key(GLFW_KEY_LEFT_ALT) },
		{ GLFW_KEY_LEFT_SUPER, new Keyboard_Key(GLFW_KEY_LEFT_SUPER) },
		{ GLFW_KEY_RIGHT_SHIFT, new Keyboard_Key(GLFW_KEY_RIGHT_SHIFT) },
		{ GLFW_KEY_RIGHT_CONTROL, new Keyboard_Key(GLFW_KEY_RIGHT_CONTROL) },
		{ GLFW_KEY_RIGHT_ALT, new Keyboard_Key(GLFW_KEY_RIGHT_ALT) },
		{ GLFW_KEY_RIGHT_SUPER, new Keyboard_Key(GLFW_KEY_RIGHT_SUPER) },
		{ GLFW_KEY_MENU, new Keyboard_Key(GLFW_KEY_MENU) },
	};

	KeyboardInput::KeyboardInput(GLFWwindow* window) : _Window(nullptr)
	{
		_Window = window;
		glfwSetKeyCallback(_Window, KeyboardKey_Callback);
	}

	KeyboardInput::~KeyboardInput()
	{
		for (auto& key : _KeyboardEvents)
			delete key.second;
		_KeyboardEvents.clear();
	}

	void KeyboardInput::KeyboardKey_Callback(GLFWwindow* window, int keyCode, int scanCode, int action, int mods)
	{
		if (_KeyboardEvents.find(keyCode) == _KeyboardEvents.end()) return;
		switch(action)
		{
		case GLFW_PRESS:
			_KeyboardEvents[keyCode]->InvokeOnPressed();
			break;
		case GLFW_REPEAT:
			_KeyboardEvents[keyCode]->InvokePressed();
			break;
		case GLFW_RELEASE:
			_KeyboardEvents[keyCode]->InvokeOnRelease();
			break;
		default: ;
		}
	}
}
