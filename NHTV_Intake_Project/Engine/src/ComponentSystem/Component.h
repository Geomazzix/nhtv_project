#pragma once
#include <glm/detail/type_mat4x4.hpp>

namespace Vesta
{
	///<summary>Base class for components within the engine.</summary>
	class Component
	{
		friend class Entity;

	private:
		bool _Active;

	protected:
		explicit Component(const bool& active) : _Active(active){}
		virtual ~Component() = default;
	
	public:
		virtual void Init() {}
		virtual void Delete(){}

		virtual void Render(const glm::mat4& mvpMatrix) {}
		virtual void Update(const float& deltaTime) {}

		virtual void OnEnable(){}
		virtual void OnDisable(){}

		bool IsActive() const { return _Active; }
		void SetActive(const bool& state)
		{
			_Active = state;
			if (state) OnEnable();
			else OnDisable();
		}
	};
}
