#include "Entity.h"

namespace Vesta
{	
	Entity::Entity(const std::string& name, const bool& active) : _Name(name), _Active(active){}

	Entity::~Entity()
	{
		for (auto& component : _Components)
			delete component;
		_Components.clear();
	}

	void Entity::SetActive(const bool& state)
	{
		_Active = state;
		if (state) OnEnable();
		else OnDisable();
	}

	void Entity::Render(const glm::mat4& viewMatrix)
	{
		if (!_Active) return;
		for (auto& component : _Components)
		{
			if (!component->IsActive()) continue;
			component->Render(viewMatrix);
		}
	}

	void Entity::Update(const float& deltaTime)
	{
		if (!_Active) return;
		for (auto& component : _Components)
		{
			if (!component->IsActive()) continue;
			component->Update(deltaTime);
		}
	}

	void Entity::Delete()
	{
		for (auto& component : _Components)
		{
			if (!component->IsActive()) continue;
			component->Delete();
		}
	}
}
