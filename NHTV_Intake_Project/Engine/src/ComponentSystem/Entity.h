#pragma once
#include <vector>
#include <glm/glm.hpp>
#include "Component.h"

namespace Vesta
{
	class Entity
	{
	private:
		bool _Active;
		std::vector<Component*> _Components;

	protected:
		std::string _Name;

		virtual void OnEnable(){}
		virtual void OnDisable(){}
		virtual void Delete();

		virtual void Update(const float& deltaTime);
		virtual void Render(const glm::mat4& viewMatrix);

		explicit Entity(const std::string& name, const bool& active = true);
		virtual ~Entity();

	public:
		bool isActive() const { return _Active; }
		void SetActive(const bool& state);

		const std::string GetName() const { return _Name; }

		template<typename T, typename... TArguments>
		T& AddComponent(TArguments&&... arguments)
		{
			T* component(new T(std::forward<TArguments>(arguments)...));
			_Components.emplace_back(component);
			component->Init();
			return *static_cast<T*>(component);
		}

		template<typename T>
		T& GetComponent()
		{
			for(unsigned int i = 0; i < _Components.size(); ++i)
			{			
				if(typeid(T) == typeid(*_Components[i]))
					return *static_cast<T*>(_Components[i]);
			}
			return *static_cast<T*>(nullptr);
		}
	};
}
