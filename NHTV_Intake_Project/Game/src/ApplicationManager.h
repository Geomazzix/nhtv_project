#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <Resources/GameObject.h>
#include <Box2D/Box2D.h>
#include <Core.h>
#include <deque>
#include <ComponentSystem/Entity.h>
#include <Components/BoxCollider2D.h>
#include "Player/PlayerObject.h"
#include "Environment/StaticColliderEnvironmentBlock.h"
#include "LevelBuilder.h"

namespace IceCave
{	
	const static float UNITSCALE_WIDTH = 16.0f, UNITSCALE_HEIGHT = 9.0f;

	class ApplicationManager : Vesta::Core
	{
	private:
		PlayerObject* _PlayerObject;
		LevelBuilder* _LevelBuilder;
		float _CameraSpeed;
		b2Vec2 _PlayerSpawnPos;

	public:
		ApplicationManager();
		~ApplicationManager() override;

	private:
		void OnRender(const glm::mat4& viewMatrix) override;
		void OnUpdate(const float& deltaTime) override;
		void OnFixedUpdate(const float& deltaTime) override;

		void LerpCameraToPlayerPos(const float& deltaTime) const;
		void UpdateLevelLines() const;
	};
}