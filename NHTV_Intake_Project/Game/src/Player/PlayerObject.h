#pragma once
#include <Resources/GameObject.h>
#include <Resources/Tilemap.h>
#include <Resources/Animation.h>
#include <Components/Circle2DCollider.h>
#include <Components/RigidBody2D.h>
#include <Components/Animator.h>
#include <Window.h>
#include <string>

namespace IceCave
{	
	class PlayerObject : public Vesta::GameObject
	{
	private:
		Vesta::Animation* _WalkAnimation, *_JumpAnimation, *_FallAnimation, *_IdleAnimation;
		Vesta::Sprite* _Sprite;
		Vesta::Animator* _Animator;
		Vesta::TileMap* _TileMap;
		Vesta::SpriteRenderer* _SpriteRenderer;
		Vesta::RigidBody2D* _RigidBody;
		Vesta::Circle2DCollider* _BoxCollider;
		float _MoveSpeed, _JumpForce;
		bool _Grounded, _DoubleJumpAvailiblity;

	public:
		PlayerObject(const std::string& playerName, const bool& active);
		~PlayerObject();

		void OnEnable() override;
		void OnDisable() override;

		void Render(const glm::mat4& viewMatrix) override;
		void Update(const float& deltaTime) override;

		void Jump();
	};
}

