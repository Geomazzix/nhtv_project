#include "PlayerObject.h"
#include <World.h>
#include <Physics/PhysicsGroup.h>

namespace IceCave
{	
	PlayerObject::PlayerObject(const std::string& playerName, const bool& active)
	: Vesta::GameObject(playerName, active), _WalkAnimation(new Vesta::Animation("PlayerWalk")), _JumpAnimation(new Vesta::Animation("PlayerJump")), 
	_FallAnimation(new Vesta::Animation("PlayerFall")), _IdleAnimation(new Vesta::Animation("PlayerIdle")), _Sprite(new Vesta::Sprite(playerName + "Sprite")), _TileMap(new Vesta::TileMap()), _MoveSpeed(3.0f), 
	_JumpForce(5.5f), _Grounded(false), _DoubleJumpAvailiblity(true)
	{
		//Create the resources.
		_Sprite->CreateMesh("Player", 1.0f, 1.0f, GL_DYNAMIC_DRAW);
		_TileMap = &Vesta::ResourceManager::LoadTileMap("PlayerSpriteSheet", "textures/playerSpriteSheet.png", 0, glm::vec2(12, 12));
		_Sprite->SetTexture(*_TileMap);
		_Sprite->SetUvs(_TileMap->GetUvs(0));

		//Add the components and colliders.
		_SpriteRenderer = &AddComponent<Vesta::SpriteRenderer>(_Sprite, "Diffuse", active);
		_RigidBody = &AddComponent<Vesta::RigidBody2D>(b2Vec2(0.0f, 0.0f), b2_dynamicBody, active);
		_RigidBody->Body()->SetFixedRotation(true);
		_RigidBody->Body()->SetSleepingAllowed(false);
		_BoxCollider = &AddComponent<Vesta::Circle2DCollider>(b2Vec2(0.0f, 0.0f), 0.45f, _RigidBody->Body(), active);
		
		//Create animations.
		_IdleAnimation->Init(_Sprite, _TileMap, 1, 0);
		_IdleAnimation->SetAnimationSpeed(10.0f);
		_IdleAnimation->SetLoop(true);

		_WalkAnimation->Init(_Sprite, _TileMap, 8, 1);
		_WalkAnimation->SetAnimationSpeed(10.0f);
		_WalkAnimation->SetLoop(true);
		
		_JumpAnimation->Init(_Sprite, _TileMap, 4, 108);
		_JumpAnimation->SetAnimationSpeed(10.0f);

		_FallAnimation->Init(_Sprite, _TileMap, 6, 110);
		_FallAnimation->SetAnimationSpeed(10.0f);

		_Animator = &AddComponent<Vesta::Animator>(_WalkAnimation, active, active);

		//Add a player tag.
		Vesta::World::AddPhysicGroup(new Vesta::PhysicsGroup(playerName));
		_RigidBody->Body()->SetUserData(Vesta::World::GetPhysicsGroup(playerName));

		//Initialize input.
		Vesta::Window::KeyboardInputManager->GetkeyEvent(GLFW_KEY_SPACE).SubscribeToOnPressed("PlayerJump", [&]() { Jump(); });
	}

	PlayerObject::~PlayerObject()
	{
		_Sprite = nullptr;
		_SpriteRenderer = nullptr;
		_BoxCollider = nullptr;
		_RigidBody = nullptr;
	}

	void PlayerObject::OnEnable()
	{
		Vesta::Window::KeyboardInputManager->GetkeyEvent(GLFW_KEY_SPACE).SubscribeToWhilePressed("PlayerJump", [&]() { Jump(); });
	}

	void PlayerObject::OnDisable()
	{
		Vesta::Window::KeyboardInputManager->GetkeyEvent(GLFW_KEY_SPACE).SubscribeToOnPressed("PlayerJump", [&]() { Jump(); });
	}

	void PlayerObject::Render(const glm::mat4& viewMatrix)
	{
		GameObject::Render(viewMatrix);
	}

	void PlayerObject::Update(const float& deltaTime)
	{
		GameObject::Update(deltaTime);

		const bool moveLeft = Vesta::Window::KeyboardInputManager->GetkeyEvent(GLFW_KEY_A).IsPressed();
		const bool moveRight = Vesta::Window::KeyboardInputManager->GetkeyEvent(GLFW_KEY_D).IsPressed();
		
		_RigidBody->Body()->SetTransform(b2Vec2(_RigidBody->Body()->GetTransform().p.x + moveLeft * -_MoveSpeed * deltaTime, _RigidBody->Body()->GetTransform().p.y), 0);
		_RigidBody->Body()->SetTransform(b2Vec2(_RigidBody->Body()->GetTransform().p.x + moveRight * _MoveSpeed * deltaTime, _RigidBody->Body()->GetTransform().p.y), 0);
		_Grounded = _RigidBody->Body()->GetLinearVelocity().y <= 0.0f && _RigidBody->Body()->GetLinearVelocity().y > -0.5f;
		
		//I don't check for 0 because the world is tiled which makes the collider fall at the edges. https://www.iforce2d.net/b2dtut/ghost-vertices
		//To solve this, I created a circle collider.

		//Check for animations.
		if (_RigidBody->Body()->GetLinearVelocity().y < -0.5f) _Animator->SetAnimation(_FallAnimation);
		else if (_Grounded && (moveLeft || moveRight)) _Animator->SetAnimation(_WalkAnimation);
		else if(_Grounded) _Animator->SetAnimation(_IdleAnimation);

		if (moveLeft)_SpriteRenderer->SetMirroredX(true);
		if (moveRight)_SpriteRenderer->SetMirroredX(false);
	}

	void PlayerObject::Jump()
	{
		if(_Grounded && !_DoubleJumpAvailiblity) _DoubleJumpAvailiblity = true;
		if (!_Grounded && !_DoubleJumpAvailiblity) return;
		if (!_Grounded) _DoubleJumpAvailiblity = false;

		_RigidBody->Body()->SetLinearVelocity(b2Vec2(_RigidBody->Body()->GetLinearVelocity().x, 0));
		_RigidBody->Body()->ApplyLinearImpulse(b2Vec2(0, _JumpForce), _RigidBody->Body()->GetWorldCenter(), true);
		_Animator->SetAnimation(_JumpAnimation, true, true);
		_Grounded = false;
	}
}
