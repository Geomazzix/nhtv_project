#include "LevelLine.h"
#include "Environment/StaticColliderEnvironmentBlock.h"
#include "Environment/EnvironmentBlock.h"

namespace IceCave
{	
	LevelLine::LevelLine(const int& lineIndex, std::array<BlockData, TILE_COUNT_IN_LINE> tiles) : _LineIndex(lineIndex), _IsActive(true)
	{
		for(unsigned short i = 0; i < TILE_COUNT_IN_LINE; ++i)
		{
			if (tiles[i].Sprite == SpriteType::EMPTY) continue;
			switch(tiles[i].Type)
			{
			case BlockType::STATIC_COLLISION:
				_Tiles[i] = new StaticColliderEnvironmentBlock("WorldLine_" + std::to_string(_LineIndex) + "_Tile_" + std::to_string(i), static_cast<int>(tiles[i].Sprite), true);
				_Tiles[i]->GetComponent<Vesta::RigidBody2D>().Body()->SetTransform(b2Vec2(i, lineIndex), 0);				
				break;

			//The reason I did not use background sprites in the end is because I didn't have time anymore to implement a layer system. 
			//Which resulted in the player being drawn behind the environment (because he is instantiated first).
			case BlockType::BACKGROUND:
				_Tiles[i] = new EnvironmentBlock("WorldLine_" + std::to_string(_LineIndex) + "_Tile_" + std::to_string(i), static_cast<int>(tiles[i].Sprite), true);
				_Tiles[i]->GetComponent<Vesta::Transform>().SetPosition(glm::vec3(i, lineIndex, 0));
				break;
			}
		}
	}

	void LevelLine::Enable()
	{
		for (auto& tile : _Tiles)
		{
			if (tile == nullptr) continue;
			tile->SetActive(true);
		}
		_IsActive = true;
	}

	void LevelLine::Disable()
	{
		for (auto& tile : _Tiles)
		{
			if (tile == nullptr) continue;
			tile->SetActive(false);
		}
		_IsActive = false;
	}
}
