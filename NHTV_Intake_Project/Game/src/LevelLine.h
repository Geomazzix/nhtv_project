#pragma once
#include <array>
#include "Resources/GameObject.h"

namespace IceCave
{

	///<summary>Holds data over the block itself, does it have collision, is there a certain action bound to it etc.</summary>
	enum class BlockType
	{
		BACKGROUND = 0,
		STATIC_COLLISION
	};

	///<summary>Holds an index to the sprite inside of the environment tilemap, but is by using this refered to as a name.</summary>
	enum class SpriteType
	{
		EMPTY = 0,

		//Snowy blocks
		SNOW_FULL_BLOCK_ROUND_EDGES = 1,
		SNOW_WITH_TOP_FULL_BLOCK_ROUND_EDGE_LEFT = 2,
		SNOW_WITH_TOP_FULL_BLOCK = 3,
		SNOW_WITH_TOP_FULL_BLOCK_ROUND_EDGE_RIGHT = 4,
		SNOW_ARC_IN_LEFT = 5,
		SNOW_FULL_BLOCK = 16,
		SNOW_ARC_IN_RIGHT = 19,

		//Spikes
		EVEN_SPIKES_DOWN = 42,
		UNEVEN_SPIKES_DOWN = 43,
		EVEN_SPIKES_UP = 44,
		UNEVEN_SPIKES_UP = 45,

		//Ice
		ICE_BLOCK_FULL = 51
	};


	///<summary>Holds the initData of an enironmentBlock.</summary>
	class BlockData
	{
	public:
		BlockType Type;
		SpriteType Sprite;
	};

	///<summary>Holds the tile data of a line from which the world is constructed.</summary>
	class LevelLine
	{
	public:
		const static unsigned short TILE_COUNT_IN_LINE = 22;

	private:
		std::array<Vesta::GameObject*, TILE_COUNT_IN_LINE> _Tiles = {
			nullptr, nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr, nullptr
		};
		int _LineIndex;
		bool _IsActive;

	public:
		LevelLine(const int& lineIndex, std::array<BlockData, TILE_COUNT_IN_LINE> tiles);
		~LevelLine() = default;

		void Enable();
		void Disable();
	
		const bool IsActive() const { return _IsActive; }
	};
}

