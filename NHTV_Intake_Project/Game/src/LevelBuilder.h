#pragma once
#include <deque>
#include "LevelLine.h"
#include <random>
#include "Environment/Win_Text.h"

namespace IceCave
{
	///<summary>Handles world building in this game.</summary>
	class LevelBuilder
	{
	private:
		const int START_LEVEL_OFFSETS = 8;
		const int MIN_LINES_BEFORE_PLATFORM = 4;
		const int MAX_LINES_BEFORE_PLATFORM = 6;
		const int ACTIVE_LEVEL_LINE_COUNT = 16;
		const int LINE_COUNT_LEVEL_END = 96;

		std::mt19937 _Num_Generator;
		std::deque<LevelLine*> _Lines;
		std::deque<LevelLine*> _ActiveLines;
		int _FrontIndex, _BackIndex, _MaxActiveLevelLines, _LinesBeforeNextPlatform;
		bool _EndGenerated;
		Win_Text* _WinTextObject;
		

		int GetRandomValue(int min, int max);
		bool ConstructPlatform();
		void AddLevelLine(std::array<BlockData, LevelLine::TILE_COUNT_IN_LINE> lineData);
		void AddFinalLine();

	public:
		LevelBuilder();
		~LevelBuilder() = default;

		void GenerateLine();

		void ShiftLineUp();
		void ShiftLineDown();

		const int GetFrontIndex() const { return _FrontIndex; }
		const int GetBackIndex() const { return _BackIndex; }
	};
}
