#pragma once
#include <Resources/GameObject.h>
#include <Components/BoxCollider2D.h>
#include <Components/RigidBody2D.h>
#include <string>
#include "EnvironmentBlock.h"

namespace IceCave
{	
	class StaticColliderEnvironmentBlock : public EnvironmentBlock
	{
	private:
		Vesta::RigidBody2D* _RigidBody;
		Vesta::BoxCollider2D* _BoxCollider;

	public:
		StaticColliderEnvironmentBlock(const std::string& name, const int& tileIndex, const bool& active);
		~StaticColliderEnvironmentBlock();

		void Render(const glm::mat4& viewMatrix) override;
		void Update(const float& deltaTime) override;
	};
}