#include "StaticColliderEnvironmentBlock.h"
#include <World.h>

namespace IceCave
{	
	StaticColliderEnvironmentBlock::StaticColliderEnvironmentBlock(const std::string& name, const int& tileIndex, const bool& active) : EnvironmentBlock(name, tileIndex, active)
	{
		_RigidBody = &AddComponent<Vesta::RigidBody2D>(b2Vec2(0.0f, 0.0f), b2_staticBody, active);
		_BoxCollider = &AddComponent<Vesta::BoxCollider2D>(b2Vec2(0.0f, 0.0f), b2Vec2(0.8f, 1.0f), _RigidBody->Body(), 0, active);
	}

	StaticColliderEnvironmentBlock::~StaticColliderEnvironmentBlock()
	{
		_BoxCollider = nullptr;
		_RigidBody = nullptr;
	}

	void StaticColliderEnvironmentBlock::Render(const glm::mat4& viewMatrix)
	{
		EnvironmentBlock::Render(viewMatrix);
	}

	void StaticColliderEnvironmentBlock::Update(const float& deltaTime)
	{
		EnvironmentBlock::Update(deltaTime);
	}
}
