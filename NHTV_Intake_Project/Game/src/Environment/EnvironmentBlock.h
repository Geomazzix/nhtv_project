#pragma once
#include <Resources/GameObject.h>

namespace IceCave
{
	///<summary>Serves as background sprite in the game.</summary>
	class EnvironmentBlock : public Vesta::GameObject
	{
	protected:
		Vesta::Sprite* _Sprite;
		Vesta::TileMap* _EnvironmentalTileMap;
		Vesta::SpriteRenderer* _SpriteRenderer;

	public:
		EnvironmentBlock(const std::string& name, const int& tileIndex, const bool& active);
		~EnvironmentBlock();

		void Render(const glm::mat4& viewMatrix) override;
		void Update(const float& deltaTime) override;
	};
}

