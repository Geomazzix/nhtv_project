#pragma once
#include <Resources/GameObject.h>

namespace IceCave
{
	///<summary>Contains a sprite displaying the win text for when the player has made it to the goal.</summary>
	class Win_Text : public Vesta::GameObject
	{
	private:
		Vesta::Sprite* _Sprite;
		Vesta::SpriteRenderer* _SpriteRenderer;

	public:
		Win_Text(std::string name, const bool active = true);
		~Win_Text();

		void Render(const glm::mat4& viewMatrix) override;
		void Update(const float& deltaTime) override;
	};
}

