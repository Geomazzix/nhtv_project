#include "Win_Text.h"


namespace IceCave
{	
	Win_Text::Win_Text(std::string name, const bool active) : Vesta::GameObject(name, active), _Sprite(new Vesta::Sprite("Win_Text"))
	{
		_Sprite->Generate(name + "Sprite", 8.0f, 1.5f, "textures/win_text.png", GL_STATIC_DRAW);
		_SpriteRenderer = &AddComponent<Vesta::SpriteRenderer>(_Sprite, "Diffuse", active);
	}


	Win_Text::~Win_Text()
	{
		_Sprite = nullptr;
		_SpriteRenderer = nullptr;
	}


	void Win_Text::Render(const glm::mat4& viewMatrix)
	{
		Vesta::GameObject::Render(viewMatrix);
	}

	void Win_Text::Update(const float& deltaTime)
	{
		Vesta::GameObject::Update(deltaTime);
	}
}
