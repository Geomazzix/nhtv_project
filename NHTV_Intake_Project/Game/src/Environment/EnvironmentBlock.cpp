#include "EnvironmentBlock.h"
#include "StaticColliderEnvironmentBlock.h"

namespace IceCave
{
	EnvironmentBlock::EnvironmentBlock(const std::string& name, const int& tileIndex, const bool& active) : GameObject(name, active), _Sprite(new Vesta::Sprite(name + "Sprite"))
	{
		_Sprite->CreateMesh(name + "Sprite", 1.0f, 1.0f);
		_EnvironmentalTileMap = &Vesta::ResourceManager::LoadTileMap("Environment_Block", "textures/environmentSpriteSheet.png", 3, glm::vec2(14, 7));
		_Sprite->SetUvs(_EnvironmentalTileMap->GetUvs(tileIndex));
		_Sprite->SetTexture(*_EnvironmentalTileMap);
		_SpriteRenderer = &AddComponent<Vesta::SpriteRenderer>(_Sprite, "Diffuse", active);
	}


	EnvironmentBlock::~EnvironmentBlock()
	{
		_Sprite = nullptr;
		_SpriteRenderer = nullptr;
		_EnvironmentalTileMap = nullptr;
	}

	void EnvironmentBlock::Render(const glm::mat4& viewMatrix)
	{
		Vesta::GameObject::Render(viewMatrix);
	}

	void EnvironmentBlock::Update(const float& deltaTime)
	{
		Vesta::GameObject::Update(deltaTime);
	}
}
