#include "LevelBuilder.h"
#include <array>


namespace IceCave
{

	LevelBuilder::LevelBuilder() : _FrontIndex(0), _BackIndex(ACTIVE_LEVEL_LINE_COUNT - 1), 
	_MaxActiveLevelLines(ACTIVE_LEVEL_LINE_COUNT), _LinesBeforeNextPlatform(0), _EndGenerated(false)
	{
		_Num_Generator.seed(std::random_device()());
		_LinesBeforeNextPlatform = GetRandomValue(MIN_LINES_BEFORE_PLATFORM, MAX_LINES_BEFORE_PLATFORM);

		//The first 6 lines is a unique one, which is therefore instantiated in the constructor.
		for(int i = 0; i < 6; ++i)
		{
			const std::array<BlockData, LevelLine::TILE_COUNT_IN_LINE> firstLineData = {
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::ICE_BLOCK_FULL}
			};
			AddLevelLine(firstLineData);
		}

		//After that start generating the rest of the world.
		for (int i = 6; i < _MaxActiveLevelLines; ++i) GenerateLine();
	}

	void LevelBuilder::GenerateLine()
	{
		if (_EndGenerated) return;
		if (_BackIndex + 1 >= LINE_COUNT_LEVEL_END && !_EndGenerated)
		{
			AddFinalLine();
			_EndGenerated = true;
			return;
		}

		const bool constructPlatform = ConstructPlatform();
		const int platformStartIndex = GetRandomValue(START_LEVEL_OFFSETS - 1, LevelLine::TILE_COUNT_IN_LINE - START_LEVEL_OFFSETS - 1);
		const int platformSize = GetRandomValue(1, LevelLine::TILE_COUNT_IN_LINE - START_LEVEL_OFFSETS * 2 - 2);

		std::array<BlockData, LevelLine::TILE_COUNT_IN_LINE> lineData = {
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY},
				BlockData{ BlockType::BACKGROUND, SpriteType::EMPTY}
		};

		short platformSizeIndex = 0;
		for(short i = 0; i < LevelLine::TILE_COUNT_IN_LINE; ++i)
		{
			//Check for left offset of level.
			if(i < START_LEVEL_OFFSETS)
			{
				lineData[i].Type = BlockType::STATIC_COLLISION;
				lineData[i].Sprite = SpriteType::ICE_BLOCK_FULL;
				continue;
			}

			//Check for right offset of level.
			if(LevelLine::TILE_COUNT_IN_LINE - START_LEVEL_OFFSETS - 1 < i)
			{
				lineData[i].Type = BlockType::STATIC_COLLISION;
				lineData[i].Sprite = SpriteType::ICE_BLOCK_FULL;
				continue;
			}

			//Check if there is supposed to be a platform constructed, if not just continue.
			if (!constructPlatform) continue;

			//Check for when the platform has to start and if it's length is longer than 1.
			if(i >= platformStartIndex && platformSizeIndex <= 0)
			{
				++platformSizeIndex;
				if (platformSize > 1)
				{
					lineData[i].Type = BlockType::STATIC_COLLISION;
					lineData[i].Sprite = SpriteType::SNOW_WITH_TOP_FULL_BLOCK_ROUND_EDGE_LEFT;
					continue;
				}
				lineData[i].Type = BlockType::STATIC_COLLISION;
				lineData[i].Sprite = SpriteType::SNOW_FULL_BLOCK_ROUND_EDGES;
				continue;
			}

			//If the platform size is bigger than 1 and it's still being constructed add a tile.
			if(platformSizeIndex < platformSize && i >= platformStartIndex && platformSize > 1)
			{
				lineData[i].Type = BlockType::STATIC_COLLISION;
				lineData[i].Sprite = SpriteType::SNOW_WITH_TOP_FULL_BLOCK;
				++platformSizeIndex;
				continue;
			}

			//Check when the platform has to end.
			if (platformSizeIndex == platformSize && platformSize > 1)
			{
				lineData[i].Type = BlockType::STATIC_COLLISION;
				lineData[i].Sprite = SpriteType::SNOW_WITH_TOP_FULL_BLOCK_ROUND_EDGE_RIGHT;
				++platformSizeIndex;
				continue;
			}
		}
		
		AddLevelLine(lineData);
	}

	int LevelBuilder::GetRandomValue(int min, int max)
	{
		const std::uniform_int_distribution<> number(min, max);
		return number(_Num_Generator);
	}

	bool LevelBuilder::ConstructPlatform()
	{
		--_LinesBeforeNextPlatform;
		if (_LinesBeforeNextPlatform <= 0)
		{
			_LinesBeforeNextPlatform = GetRandomValue(MIN_LINES_BEFORE_PLATFORM, MAX_LINES_BEFORE_PLATFORM);
			return true;
		}
		return false;
	}

	void LevelBuilder::AddLevelLine(std::array<BlockData, LevelLine::TILE_COUNT_IN_LINE> lineData)
	{
		LevelLine* line = new LevelLine(_Lines.size(), lineData);
		_Lines.push_back(line);
		if (static_cast<int>(_ActiveLines.size()) >= _MaxActiveLevelLines) ShiftLineUp();
		else _ActiveLines.push_back(_Lines[_Lines.size() - 1]);
	}

	void LevelBuilder::AddFinalLine()
	{
		const std::array<BlockData, LevelLine::TILE_COUNT_IN_LINE> caveLine =
		{
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_ARC_IN_RIGHT},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::EMPTY},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::EMPTY},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::EMPTY},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::EMPTY},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_ARC_IN_LEFT},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_FULL_BLOCK}
		};
		AddLevelLine(caveLine);

		const std::array<BlockData, LevelLine::TILE_COUNT_IN_LINE> lastCaveLine =
		{
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_ARC_IN_RIGHT},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::EMPTY},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::EMPTY},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_ARC_IN_LEFT},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK},
				BlockData{ BlockType::STATIC_COLLISION, SpriteType::SNOW_WITH_TOP_FULL_BLOCK}
		};
		AddLevelLine(lastCaveLine);
		_WinTextObject = new Win_Text("WinText", true);
		_WinTextObject->GetComponent<Vesta::Transform>().SetPosition(glm::vec3(11, LINE_COUNT_LEVEL_END + 3, 0));
	}

	void LevelBuilder::ShiftLineUp()
	{
		if (static_cast<unsigned int>(_BackIndex + 1) > _Lines.size() - 1)
		{
			GenerateLine();
			return;
		}

		_ActiveLines[0]->Disable();
		_ActiveLines.pop_front();
		++_FrontIndex;
		++_BackIndex;
		_ActiveLines.push_back(_Lines[_BackIndex]);
		_ActiveLines[_ActiveLines.size() - 1]->Enable();
	}

	void LevelBuilder::ShiftLineDown()
	{
		if (_FrontIndex - 1 < 0) return;

		_ActiveLines[_ActiveLines.size() - 1]->Disable();
		_ActiveLines.pop_back();
		--_FrontIndex;
		--_BackIndex;
		_ActiveLines.push_front(_Lines[_FrontIndex]);
		_ActiveLines[0]->Enable();
	}
}
