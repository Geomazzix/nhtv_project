#include "ErrorHandler.h"
#include "ApplicationManager.h"
#include <cstdio>
#include <glm/glm.hpp>
#include <Box2D/box2D.h>

namespace IceCave
{
	//The constructor of The Core initializes GLFW, the window, the input and GLEW.
	ApplicationManager::ApplicationManager() : Core("window title", UNITSCALE_WIDTH * 100, UNITSCALE_HEIGHT * 100, Vesta::Window::WINDOWED, b2Vec2(UNITSCALE_WIDTH, UNITSCALE_HEIGHT), true)
	{
		_ERender.Event->Subscribe("ApplicationRender", [&](const glm::mat4 viewMatrix) { this->OnRender(viewMatrix); });
		_EUpdate.Event->Subscribe("ApplicationUpdate", [&](const float deltaTime) { this->OnUpdate(deltaTime); });
		_EFixedUpdate.Event->Subscribe("ApplicationRender", [&](const float deltaTime) { this->OnFixedUpdate(deltaTime); });

		//Load the first scene in.
		Vesta::SceneManager::LoadScene("Level");

		//Create the player and it's environment.
		_PlayerSpawnPos = b2Vec2(8.0f, 6.0f);
		_PlayerObject = new PlayerObject("PlayerObject", true);
		_PlayerObject->GetComponent<Vesta::RigidBody2D>().Body()->SetTransform(_PlayerSpawnPos, 0);
		
		_Camera->SetPosition(glm::vec2(_PlayerSpawnPos.x + 2.5f, _PlayerSpawnPos.y));
		_CameraSpeed = 5.0f;

		_LevelBuilder = new LevelBuilder();

		//The init method starts the update and render calls.
		Core::Init();
	}

	//The engine handles garbage memory and object deletion, if the user wishes to explicitly remove an object he can do that by calling Vesta::World::RemoveObject.
	ApplicationManager::~ApplicationManager()
	{

	}

	void ApplicationManager::OnRender(const glm::mat4& viewMatrix)
	{

	}

	void ApplicationManager::OnUpdate(const float& deltaTime)
	{
		UpdateLevelLines();
	}

	void ApplicationManager::OnFixedUpdate(const float& deltaTime)
	{
		LerpCameraToPlayerPos(deltaTime);
	}

	void ApplicationManager::LerpCameraToPlayerPos(const float& deltaTime) const
	{
		const float camSpeed = _CameraSpeed * deltaTime;
		const glm::vec2 followPos = glm::vec2(_Camera->GetPosition().x, _PlayerObject->GetComponent<Vesta::Transform>().GetPosition().y);
		const glm::vec2 newCamPos = _Camera->GetPosition() + ((followPos - _Camera->GetPosition()) * camSpeed);
		_Camera->SetPosition(newCamPos);
	}

	void ApplicationManager::UpdateLevelLines() const
	{
		const float playerY = _PlayerObject->GetComponent<Vesta::Transform>().GetPosition().y;
		if (playerY > (_LevelBuilder->GetFrontIndex() + _LevelBuilder->GetBackIndex()) / 2 + 1)
			_LevelBuilder->ShiftLineUp();
		else if (playerY < (_LevelBuilder->GetFrontIndex() + _LevelBuilder->GetBackIndex()) / 2 - 1)
			_LevelBuilder->ShiftLineDown();
	}
}
